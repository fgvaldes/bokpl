# Crosstalk files.

# Common values.
CLASS = "'xtalkfile'"
DIR = "'misc'"
DATATYPE = "'file'"
MATCH = 1

MJD,MJDWIN1,MJDWIN2,VALUE

#57160,0,267,'xtalk150413.txt'
#57428,NULL,NULL,'xtalk2016_cn.txt'
57160,0,960,'xtalk160209.txt'
58121,0,344,'xtalk_DH1801.txt'
58465,-1,NULL,'xtalk_DH181212.txt'
#57160,0,595,'xtalk160209.txt'
#57755,0,NULL,'xtalk2017a.txt'
