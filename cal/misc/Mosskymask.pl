  �V  %�   �$TITLE = ""
$CTIME = 1142077227
$MTIME = 1142077227
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
DATE    = '2016-03-10T16:37:33' / Date FITS file was generated
IRAF-TLM= '2016-03-10T16:37:53' / Time of last modification
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
FILENAME= 'mos3.66299.fits'    / Original host filename
EXPNUM  =                66299 / Exposure sequence number
OBSTYPE = 'object  '           / Observation type
EXPTIME =              250.103 / Exposure time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '06:56:35.76'        / RA of observation (hr)
DEC     = '38:37:37.20'        / DEC of observation (deg)
OBJRA   = '06:56:35.76'        / Right Ascension
OBJDEC  = '38:37:37.20'        / Declination

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2016-02-07T05:13:10.0' / Date of observation start (UTC)
TIME-OBS= '05:13:10'           / Universal time
MJD-OBS =       57425.21747685 / MJD of observation start
ST      = '06:58:36'           / Sidereal time
MJDSTART=      57425.217491867 / MJD of observation start
MJDEND  =      57425.220717667 / MJD of observation end

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=                 2000 / Equinox of tel coords
TELRA   = '06:56:35.76'        / RA of telescope (hr)
TELDEC  = '38:37:37.20'        / DEC of telescope (deg)
TELFOCUS=                -9538 / Telescope focus
HA      = '-00:03:49.66'       / Telescope hour angle
ZD      =                 6.68 / Zenith distance
AIRMASS =                1.008 / Airmass
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off     '           / ADC Mode
ADCPAN1 =                 0.04 / [deg] MSE ADC 1 prism angle
ADCPAN2 =                 0.04 / [deg] MSE ADC 2 prism angle
CORRCTOR= 'Mayall Corrector'   / Corrector Identification

INSTRUME= 'Mosaic3 '           / Science Instrument
DETECTOR= 'Mosaic3 '           / Instrument detector
DETSIZE = '[1:8224,1:8192]'    / Mosaic detector size
NCCDS   =                    4 / Number of CCDs in mosaic
NAMPS   =                   16 / Number of amplifiers in mosaic
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILTER  = 'zd DECam k1038'     / Filter name
FILTPOS =                   14 / Instrument filter position
FILTERSN= 'k1038   '           / Filter serial number
SHUTSTAT= 'guide   '           / Shutter status
TV1FOC  =                2.793 / [mm] MSE camera 1 focus (value)
TV2FOC  =               -0.598 / [mm] MSE camera 2 focus (value)
ENVTEM  =                   13 / [celsius] MSE temp7 - Ambient
DEWAR   = 'Mosaic3 Dewar'      / Dewar identification
CCDTEMP1=             -102.607 / [deg] CCD temperature 1
CCDTEMP2=             -105.808 / [deg] CCD temperature 1
CCDTEMP3=             -106.546 / [deg] CCD temperature 1
CCDTEMP4=             -104.085 / [deg] CCD temperature 1

OBSERVER= 'Arjun Dey, Gautham Narayan, Xu Zhou' / Observer(s)
PROPOSER= 'Arjun Dey'          / Proposer(s)
PROPID  = '2016A-0453'         / Proposal identification
OBSID   = 'kp4m.20160207T051310' / Observation ID
EXPID   =                    0 / Monsoon exposure ID
NOCID   =      2457425.9258139 / NOCS exposure ID

CONTROLR= 'Mosaic System Electronics  Sep 2' / MSE name and revision date
READMODE= 'fastMode'           / Readout mode
NSAMPSIG=                  176 / Sampling number
GAINFACT=                    7 / Controler gain factor
READTIME=            28.014493 / Read out time
DHEFILE = 'mosaic3_Seq2amp250Kpx.ucd' / Sequencer file

NOCMDOF =                    0 / [arcsec] Map Dec offset
NOCMITER=                    0 / Map iteration count
NOCNO   =                    1 / observation number in this sequence
NOCPOST = 'dfs     '           / ntcs_moveto ra dec epoch
NOCDROF =                    0 / [arcsec] Dither RA offset
NOCDHS  = 'OBJECT  '           / DHS script name
NOCGPXPS=                   30 / Monsoon pixel row/column shift
NOCFSTEP=                 -100 / [um] step value for focus adjustments
NOCORA  =                    0 / [arcsec] RA offset
NOCODEC =                    0 / [arcsec] Dec offset
NOCMPOS =                    0 / Map position
NOCDITER=                    0 / Dither iteration count
NOCFITER=                    9 / Number of focus positions
NOCMPAT = 'unknown '           / Map pattern
NOCMREP =                    0 / Map repetition count
NOCDDOF =                    0 / [arcsec] Dither Dec offset
NOCTOT  =                    1 / Total number of observations in set
NOCSCR  = 'MzLS_597239_z'      / NOHS script run
NOCFOCUS=                -9100 / [um] nics_focus value
NOCTIM  =                  250 / [s] Requested integration time
NOCOFFT = '0 0     '           / ntcs_offset RA Dec offset (arcsec)
NOCSYS  = 'kpno 4m '           / system ID
NOCNUM  =                    1 / observation number request
NOCLAMP = 'On      '           / Dome flat lamp status (on|off|unknown)
NOCRBIN =                    1 / CCD row binning
NOCMROF =                    0 / [arcsec] Map RA offset
NOCSKY  =                    0 / sky offset modulus
NOCNPOS =                    1 / observation number in requested number
NOCCBIN =                    1 / CCD column binning
NOCTYP  = 'OBJECT  '           / Observation type
NOCDPOS =                    0 / Dither position
NOCDPAT = 'unknown '           / Dither pattern
NOCDREP =                    0 / Dither repetition count

RAINDEX =                    0 / [arcsec] RA index
RAZERO  =                    0 / [arcsec] RA zero
ALT     = '82:46:30.72'        / Telescope altitude
DECINST =                    0 / [arcsec] Dec instrument center
DECDIFF =                    0 / [arcsec] Dec diff
FOCUS   =                -9538 / [mm] Telescope focus
PARALL  =                181.7 / [deg] parallactic angle
DECZERO =                    0 / [arcsec] Dec zero
AZ      = '01:32:42.36'        / Telescope azimuth
RADIFF  =                    0 / [arcsec] RA diff
RAINST  =                    0 / [arcsec] RA instrument center
DECOFF  =                    0 / [arcsec] Dec offset
DECINDEX=                    0 / [arcsec] Dec index
RAOFF   =                    0 / [arcsec] RA offset

MSEPEDF =                -9538 / [microns] MSE pedestal focus position

MSEREADY= 'guide   '           / MSE shutter ready (none|guide|dark|restore)

TCPGDR  = 'on      '           / Guider status (on|off|lock)
PRODTYPE= 'image   '
DTACQNAM= '/data1/batch/mosaic3/20160206/mos3.66299.fits.fz'
DTTELESC= 'kp4m    '
PROCTYPE= 'raw     '
DTINSTRU= 'mosaic3 '
ODATEOBS= '2016-02-07T05:13:10.0'
DTPROPID= '2016A-0453'
DTSITE  = 'kp      '
DTNSANAM= 'k4m_160207_051310_ori.fits.fz'
DTCALDAT= '2016-02-06'

CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:4112,1:4096]'    / CCD section

LTM1_1  =  0.00490196095779538 / CCD to image transformation
LTM2_2  =  0.00490196095779538 / CCD to image transformation
DTM1_1  =                    1 / CCD to detector transformation
DTM2_2  =                    1 / CCD to detector transformation

WCSASTRM= 'kp4m.20151027T113418 by Valdes 2015-12-14' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---ZPX'           / Coordinate type
CTYPE2  = 'DEC--ZPX'           / Coordinate type
CRVAL1  =              104.149 / Coordinate reference value
CRVAL2  =               38.627 / Coordinate reference value
XTALKFIL= 'xtalk_160111.txt'
GAINNORM=          0.007197035
WAT0_001= 'system=image'
WAT1_001= 'wtype=zpx axtype=ra projp0=0. projp1=1. projp2=0. projp3=337.74 proj'
WAT2_001= 'wtype=zpx axtype=dec projp0=0. projp1=1. projp2=0. projp3=337.74 pro'
PROCID  = 'kp4m.20160207T051310V2'
LTV1    =                  0.5
LTV2    =                  0.5
WCSCAL  = 'Successful'         / WCS calibration
ASTRMCAT= '2MASS   '           / Astrometric ref. catalog
PHOTCAL = 'Successful'         / Photometric calibration
PHOTREF = 'PS1     '           / Phototometric reference catalog
PHOTBAND= 'z       '           / Photometric reference band
PHOTFILT= 'z       '           / Photometric reference band
MAGZERO =                25.94 / Magnitude zeropoint
MAGZSIG =                0.077 / Magnitude zeropoint sigma
MAGZNAV =                 2417 / Magnitude zeropoint sources
DQOBMAXA=                 4531 / Maximum object area (pix)
DQSKFRAC=                0.971 / Sky coverage fraction
SKY     = '56.77387  2.012473' / Sky statistics
SKYMEAN =             56.77387 / Sky mean
SIG     = '0.9765328  0.1878961' / Sky sigma statistics
SIGMEAN =            0.9765328 / Sky sigma mean
SFTFLAG = 'Y       '           / Dark sky flat flag
FRGFLAG = 'Y       '           / Fringe flag
PGRFLAG = 'Y       '           / Pupil ghost removal flag
SEEINGP =                 4.06 / [pix] Seeing
SEEING  =                 1.05 / [arcsec] Seeing
SKYADU  =               56.774 / Sky (BUNIT)
SKYMAG  =                18.61 / [mag/arcsec^2] sky brightness
SKYNOISE=              0.97653 / Sky noise (BUNIT)
DPTHADU =                23.64 / Depth (BUNIT)
PHOTDPTH=                22.51 / [mag] 5 sig point source photometric depth
   �      (   (                  �   �   (      �       ���               ��       ` @`	``` @```` @`	   ��       (  ��      @(  �� 	   	  @P'  ��      @ @P  ��      @ @P  ��      @ @
P  ��      @ @P  ��      @ @P  ��      @ @P  ��      @ @
P  ��      @ @P  ��      @ @P  �� 	   	  @P'  ��      @(