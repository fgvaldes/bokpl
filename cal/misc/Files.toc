# Text files that only depend on the instrument.

# Common values.
DIR = "'misc'"
DATATYPE = "'file'"

CLASS,VALUE

\'ccdnames'  ,'ccdnames.txt'
\'mscinstr'  ,'mscinstr.txt'
\'mscsubsets','mscsubsets.txt'
\'mscamps'   ,'mscamps.txt'
\'omisky'    ,'omisky.txt'
