# Amplifier info within CCDs.

# Common values.
CLASS = "'ampinfo'"
DIR = "'misc'"
DATATYPE = "'file'"

IMAGEID,MJD,MJDWIN1,MJDWIN2,VALUE

\'ccd1',57755,NULL,NULL,'ampinfo1_160112.txt'
\'ccd2',57755,NULL,NULL,'ampinfo2_160112.txt'
\'ccd3',57755,NULL,NULL,'ampinfo3_160112.txt'
\'ccd4',57755,NULL,NULL,'ampinfo4_160112.txt'
