# Text files.

# Common values.
DIR = "'misc'"
DATATYPE = "'file'"
MJDWIN1 = -3000
MJDWIN2 = 3000

CLASS,VALUE

\'ampinfo'   ,'ampinfo160112.txt'
\'ccdnames'  ,'ccdnames.txt'
\'mscinstr'  ,'mscinstr.txt'
\'mscsubsets','mscsubsets.txt'
\'mscamps'   ,'mscamps.txt'
