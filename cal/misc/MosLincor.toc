# Linearity correction tables.

# Common values.
CLASS = "'lincor'"
DIR = "'misc'"
DATATYPE = "'file'"

IMAGEID,MJD,MJDWIN1,MJDWIN2,VALUE

\'ccd2:B',57388,NULL,NULL,'lin2B_1603.txt'
\'ccd2:A',57388,NULL,NULL,'lin2A_1603.txt'
\'ccd2:D',57388,NULL,NULL,'lin2D_1603.txt'
\'ccd2:C',57388,NULL,NULL,'lin2C_1603.txt'
\'ccd1:B',57388,NULL,NULL,'lin1B_1603.txt'
\'ccd1:A',57388,NULL,NULL,'lin1A_1603.txt'
\'ccd1:D',57388,NULL,NULL,'lin1D_1603.txt'
\'ccd1:C',57388,NULL,NULL,'lin1C_1603.txt'
\'ccd4:C',57388,NULL,NULL,'lin4C_1603.txt'
\'ccd4:D',57388,NULL,NULL,'lin4D_1603.txt'
\'ccd4:A',57388,NULL,NULL,'lin4A_1603.txt'
\'ccd4:B',57388,NULL,NULL,'lin4B_1603.txt'
\'ccd3:C',57388,NULL,NULL,'lin3C_1603.txt'
\'ccd3:D',57388,NULL,NULL,'lin3D_1603.txt'
\'ccd3:A',57388,NULL,NULL,'lin3A_1603.txt'
\'ccd3:B',57388,NULL,NULL,'lin3B_1603.txt'
