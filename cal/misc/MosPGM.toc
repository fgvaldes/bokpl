# Pupil Ghost Masks.

# Common values.
CLASS = "'pgm'"
DIR = "'misc'"
MJD = 57370
MATCH = "'1'"
DATATYPE = "'image'"

IMAGEID,VALUE

\'LBL-01','pgm-1.pl'
\'LBL-02','pgm-2.pl'
\'LBL-03','pgm-3.pl'
\'LBL-04','pgm-4.pl'
\'ccd1','pgm-1.pl'
\'ccd2','pgm-2.pl'
\'ccd3','pgm-3.pl'
\'ccd4','pgm-4.pl'
