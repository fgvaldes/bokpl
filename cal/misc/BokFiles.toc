# Text files for 90prime

# Common values.
DETECTOR = "'90prime'"
DIR = "'90prime'"
DATATYPE = "'file'"
MJDWIN1 = -3000
MJDWIN2 = 3000

CLASS,MJD,VALUE

\'ccdnames',NULL,'ccdnames.txt'
\'mscinstr',NULL,'mscinstr.txt'
\'mscsubsets',NULL,'mscsubsets.txt'
\'mscamps',NULL,'mscamps.txt'
