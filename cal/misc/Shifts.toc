# Text files that only depend on the instrument.

# Common values.
DIR = "'misc'"
CLASS = "'shift'"
DATATYPE = "'keyword'"

IMAGEID,MJD,MJDWIN1,MJDWIN2,VALUE

#\'1',57450,NULL,0,'0.24'
#\'2',57450,NULL,0,'0.09'
#\'3',57450,NULL,0,'0.09'
#\'4',57450,NULL,0,'0.09'
\'1',57450,NULL,NULL,'0.36'
\'2',57450,NULL,NULL,'0.16'
\'3',57450,NULL,NULL,'0.16'
\'4',57450,NULL,NULL,'0.16'
