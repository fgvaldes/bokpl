  �V  ,i  t$TITLE = "flat"
$CTIME = 1122030617
$MTIME = 1122030617
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
EXTEND  =                    F / File may contain extensions
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2015-07-22T17:57:32' / Date FITS file was generated
IRAF-TLM= '2015-07-22T18:08:53' / Time of last modification
OBJECT  = 'flat    '           / Name of the object observed
NEXTEND =                   16 / Number of extensions
CCDBIN1 =                    1 / Binning factor along axis 1
CCDBIN2 =                    1 / Binning factor along axis 2
FILENAME= 'd7126.0011.fits'    / base filename at acquisition
NCCDS   =                    4 / Number of CCDs
NAMPS   =                   16 / Number of amplifiers
DARKCUR = '10.0    '           / Average dark current (e-/pixel/hour)
DARKTIME=                3.101 / Dark time (seconds)
DATE-OBS= '2015-04-14T00:44:08.530' / UTC shutter opened
DETNAME = '90prime '           / detector name
EXPREQ  =                  3.0 / Exposure time requested (seconds)
EXPTIME =                  3.0 / Exposure time (seconds)
GAIN1   = '1.3     '           / Conversion gain (e-/DN) for amp 1
GAIN10  = '1.4     '           / Conversion gain (e-/DN) for amp 10
GAIN11  = '1.4     '           / Conversion gain (e-/DN) for amp 11
GAIN12  = '1.3     '           / Conversion gain (e-/DN) for amp 12
GAIN13  = '1.4     '           / Conversion gain (e-/DN) for amp 13
GAIN14  = '1.3     '           / Conversion gain (e-/DN) for amp 14
GAIN15  = '1.4     '           / Conversion gain (e-/DN) for amp 15
GAIN16  = '1.4     '           / Conversion gain (e-/DN) for amp 16
GAIN2   = '1.3     '           / Conversion gain (e-/DN) for amp 2
GAIN3   = '1.3     '           / Conversion gain (e-/DN) for amp 3
GAIN4   = '1.3     '           / Conversion gain (e-/DN) for amp 4
GAIN5   = '1.5     '           / Conversion gain (e-/DN) for amp 5
GAIN6   = '1.5     '           / Conversion gain (e-/DN) for amp 6
GAIN7   = '1.3     '           / Conversion gain (e-/DN) for amp 7
GAIN8   = '1.5     '           / Conversion gain (e-/DN) for amp 8
GAIN9   = '1.4     '           / Conversion gain (e-/DN) for amp 9
IMAGETYP= 'flat    '           / Image type
INSTRUME= '90prime '           / Instrument name
LOCTIME = '17:44:08'           / Local time at start of exposure
OBSERVER= 'Hu Zou, Jiali Wang, Minghao Yue/ Observer names'
OPERATOR= 'MtnOps  '           / Telescope operator name
PCIFILE = 'pci3.lod'           / PCI board DSP code filename
PIXSCAL1= '0.455   '           / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2= '0.455   '           / Pixel scale for axis 2 (arcsec/pixel)
PIXSIZE1= '15.0    '           / Pixel size for axis 1 (microns)
PIXSIZE2= '15.0    '           / Pixel size for axis 2 (microns)
RDNOIS1 = '6.6     '           / Read noise (e-) for amp 1
RDNOIS10= '6.3     '           / Read noise (e-) for amp 10
RDNOIS11= '8.0     '           / Read noise (e-) for amp 11
RDNOIS12= '8.6     '           / Read noise (e-) for amp 12
RDNOIS13= '9.5     '           / Read noise (e-) for amp 13
RDNOIS14= '6.0     '           / Read noise (e-) for amp 14
RDNOIS15= '6.1     '           / Read noise (e-) for amp 15
RDNOIS16= '6.8     '           / Read noise (e-) for amp 16
RDNOIS2 = '6.7     '           / Read noise (e-) for amp 2
RDNOIS3 = '6.9     '           / Read noise (e-) for amp 3
RDNOIS4 = '6.3     '           / Read noise (e-) for amp 4
RDNOIS5 = '7.8     '           / Read noise (e-) for amp 5
RDNOIS6 = '8.2     '           / Read noise (e-) for amp 6
RDNOIS7 = '6.7     '           / Read noise (e-) for amp 7
RDNOIS8 = '6.9     '           / Read noise (e-) for amp 8
RDNOIS9 = '9.9     '           / Read noise (e-) for amp 9
READ_SPD= '138     '           / Readout speed (kpixel/sec/amp)
SITEELEV= '2120    '           / Altitude (meters)
SITELAT = '31:58.8 '           / Latitude (degrees N)
SITELONG= '111:36.0'           / Longitude (degrees W)
TELESCOP= 'Steward 2.3 m (bok)' / Telescope name
TIME-OBS= '00:44:08.530'       / UTC at start of exposure
TIMESYS = 'UTC     '           / Time system
TIMEZONE=                    7 / Local time zone
TIMFILE = '90Prime_config0.lod' / Timing board DSP code filename
UT      = '00:44:08.530'       / UTC at start of exposure
UTC-OBS = '00:44:08.530'       / UTC at start of exposure
ITL-HEAD= 'OK      '           / ITL Header flag
N-AMPS-X=                    4 / Number of amplifiers in X
N-AMPS-Y=                    4 / Number of amplifiers in Y
N-DET-X =                    2 / Number of detectors in X
N-DET-Y =                    2 / Number of detectors in Y
NUM-AMPX=                    4 / Number of amplifiers in X
NUM-AMPY=                    4 / Number of amplifiers in Y
NUM-DETX=                    2 / Number of detectors in X
NUM-DETY=                    2 / Number of detectors in Y
REF-PIX1=              4091.04 / Reference pixel 1
REF-PIX2=              4277.99 / Reference pixel 2
SYS-NAME= '90Prime '           / System name
FILTER  = 'g       '           / Filter name
FOCUSVAL= '*0.688*0.542*0.815' / Focus
TEMPS0  = '67.1 30 35.0'       / TEMP_F HUMID_% DEWPOINT_F (OUTSIDE)
TEMPS1  = '64.4 31 33.8'       / TEMP_F HUMID_% DEWPOINT_F (INSIDE)
TEMPS2  = '52.3 50 34.5'       / TEMP_F HUMID_% DEWPOINT_F (MIRROR_CELL)
TEMPS3  = '62.4 30 31.5'       / TEMP_F HUMID_% DEWPOINT_F (UPPER_DOME)
TEMPS4  =                 50.8 / TEMP_F (PRIMARY)
TEMPS5  =                 58.8 / TEMP_F (NORTH_STRUT)
TEMPS6  =   -99.90000000000001 / TEMP_F (90_PRIME)
WEATHER0=    9.779999999999999 / WIND_SPEED_MPH
WEATHER1=                24.31 / WIND_DIR_DEG
WEATHER2=    67.09999999999999 / TEMP_F
WEATHER3=                   30 / HUMID_%
AIRMASS =                 1.56 / airmass
AZIMUTH =                180.0 / azimuth
DEC     = '-18:11:33.4 '       / declination
ELEVAT  =                 40.0 / elevation
EPOCH   =               2000.0 / equinox of RA and DEC
HA      = '-00:00:07 '         / hour angle
JULIAN  =            2457126.5 / julian date
LST-OBS = '06:45:13 '          / local siderial time
MOTION  =                    0 / motion flag
RA      = '06:44:32.67 '       / right ascension
ROTANGLE=                  0.0 / IIS rotation angle
ST      = '06:45:13 '          / local siderial time
CAMTEMP =             -134.393 / Camera temperature in C
DEWTEMP =             -193.131 / Dewar temperature in C
DTCOPYRI= 'University of Arizona'
DTPI    = 'Xiaohui Fan'
DTPROPID= '2015A-0801'
PROPID  = '2015A-0801'
DTTITLE = 'Beijing-Arizona Sky Survey (BASS)'
OBSID   = 'bok23m.2015-04-14T00:44:08.530'
OBSERVAT= 'Steward '
PROCTYPE= 'Raw     '
DTSITE  = 'kp      '
DTOBSERV= 'University of Arizona Observatories'
PRODTYPE= 'Image   '
DTPIAFFL= 'University of Arizona'
DTTELESC= 'bok23m  '
OBSTYPE = 'flat    '
DTCALDAT= '2015-04-13'
ODATEOBS= '2015-04-14'
DTINSTRU= '90prime '
DTNSANAM= 'ksb_150414_004408_fri.fits.fz'
MJD-OBS =     57126.0306542814
XTALKFIL= 'fmixtalk_xtalkfile.tmp'
BUNIT   = 'ADU     '           / Physical unit of array values
COMMENT ==================================================================
COMMENT Image
COMMENT ==================================================================
IMAGEID =                   11 / Image ID
CCDNAME = 'ccd3    '           / CCD name
DETSIZE = '[1:8064,1:8192]'    / Detector size
CCDSIZE = '[1:4032,1:4096]'    / CCD size
AMPSEC  = '[1:2016,2048:1]'    / Amplifier section
DETSEC  = '[1:2016,8192:6145]' / Detector section
CCDSEC  = '[1:2016,4096:2049]' / CCD section
CCDSEC1 = '[1:2016,4096:2049]' / CCD section with binning
OVRSCAN1=                   20 / Overscan on axis 1
OVRSCAN2=                    0 / Overscan on axis 2
PRESCAN1=                    0 / Underscan on axis 1
PRESCAN2=                    0 / Underscan on axis 2
CCDSUM  = '1 1     '           / CCD pixel summing
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                 -1.0 / CCD to image transformation
LTV2    =                4097. / CCD to image transformation
ATM1_1  =                    1 / CCD to amplifier transformation
ATM2_2  =                   -1 / CCD to amplifier transformation
ATV1    =                    0 / CCD to amplifier transformation
ATV2    =                 8193 / CCD to amplifier transformation
DTM1_1  =                    1 / CCD to detector transformation
DTM2_2  =                    1 / CCD to detector transformation
DTV1    =                    0 / CCD to detector transformation
DTV2    =                    0 / CCD to detector transformation
COMMENT ==================================================================
COMMENT WCS
COMMENT ==================================================================
EQUINOX =                 2000 / Equinox of WCS
WCSDIM  =                    2 / WCS Dimensionality
CTYPE1  = 'DEC--TAN'           / Coordinate type
CTYPE2  = 'RA---TAN'           / Coordinate type
CRVAL2  =           101.136125 / Coordinate reference value
CRVAL1  =   -18.19261111111111 / Coordinate reference value
CRPIX1  =              4090.04 / Coordinate reference pixel
CRPIX2  =     181.010000000001 / Coordinate reference pixel
CD1_1   =          -0.00012638 / Coordinate matrix
CD2_2   =           0.00012638 / Coordinate matrix
COMMENT ==================================================================
COMMENT ITL Focal plane
COMMENT ==================================================================
AMP-CFG =                    2 / Amplifier configuration
DET-NUM =                    3 / Detector number
EXT-NUM =                   11 / Extension number
JPG-EXT =                   13 / Image section
DET-POSX=                    1 / Detector position in X
DET-POSY=                    2 / Detector position in Y
EXT-POSX=                    1 / Amplifier position in X
EXT-POSY=                    4 / Amplifier position in Y
AMP-PIX1=                    1 / Amplifier pixel position in X
AMP-PIX2=                 8555 / Amplifier pixel position in Y
CHECKSUM= 'AJdOAJdNAJdNAJdN'   / HDU checksum updated 2015-07-14T16:39:34
DATASUM = '2014745805'         / data unit checksum updated 2015-07-14T16:39:34
XTALKCOR= 'Jul 14  9:46 No crosstalk correction required'
AMPID   = 'IM11    '
AMPNUM  =                   11
CCDNUM  =                    3
ZERO    = 'BOK15A_20150413_890841e-ksb150414001106Z-11.fits'
CDELT1  =  -1.2638000000000E-4
CDELT2  =  1.26380000000000E-4
WAT0_001= 'system=image'
WAT1_001= 'wtype=tan axtype=dec'
WAT2_001= 'wtype=tan axtype=ra'
OVSNMEAN=              2086.23
TRIM    = 'Jul 14  9:50 Trim is [1:2016,1:2048]'
OVERSCAN= 'Jul 14  9:50 Overscan is [2017:2036,1:2048], mean 2086.23'
ZEROCOR = 'Jul 14  9:50 Zero is BOK15A_20150413_890841e-ksb150414001106Z-11.fit'
CCDMEAN =         17246.869375
CCDPROC = 'Jul 14  9:50 CCD processing done'
PROCID  = 'bok23m.2015-04-14T00:44:08.530V1'
IMCMB001= 'ksb_150414_004408_fri.fits.fz'
IMCMB002= 'ksb_150414_004450_fri.fits.fz'
IMCMB003= 'ksb_150414_004533_fri.fits.fz'
IMCMB004= 'ksb_150414_004615_fri.fits.fz'
IMCMB005= 'ksb_150414_004657_fri.fits.fz'
IMCMB006= 'ksb_150414_004740_fri.fits.fz'
IMCMB007= 'ksb_150414_004822_fri.fits.fz'
IMCMB008= 'ksb_150414_004905_fri.fits.fz'
IMCMB009= 'ksb_150414_004947_fri.fits.fz'
IMCMB010= 'ksb_150414_005029_fri.fits.fz'
NCOMBINE=                   10
QUALITY =                   1.
DCCDMEAN=             0.967277
GAINMEAN=                  1.3
    �     �                            J  t      ����               �� J      ` @`````````` @ @ @ @ @` @ @ @ @`` @`````````` F: @  @�```````````` @�`   ��      �  ��      @�@	J@ �  ��      @�@J@ �  ��      @RE�@J@ �  ��      @=@}@J@ �  ��      @;@{@J@ �  ��      @9@y@J@ �  ��      @7@w@J@ �  ��      @5@u@J@ �  ��      @4@!t@J@ �  ��      @3@#s@J@ �  ��      @2@%r@J@ �  ��      @1@'q@J@ �  ��      @0@)p@J@ �  ��      @/@+o@J@ �  ��      @.@-n@J@ �  ��      @-@/m@J@ �  ��      @,@1l@J@ �  ��      @+@3k@J@ �  ��      @,@1l@J@ �  ��      @,@1m@J@ �  ��      @-@/n@J@ �  ��      @.@-o@J@ �  ��      @/@+p@J@ �  ��      @/@+p@K@ �  ��      @0@)q@K@ �  ��      @1@'r@K@ �  ��      @2@%s@K@ �  ��      @3@#t@K@ �  ��      @4@!u@K@ �  ��      @5@v@K@ �  ��      @7@x@K@ �  ��      @9@z@K@ �  ��      @;@|@K@ �  ��      @=@~@K@ �  ��      @RE�@K@ �: ��      @�@K@ �   ��      @�@J@ � � ��      @�@K@ �  ��      @S^ l@K@ �  ��      @[@ l@K@ �  ��      @Y@ l@K@ �  ��      @V@ m@K@ �  ��      @T@ o@K@ �  ��      @R@ q@K@ �  ��      @P@ t@K@ �  ��      @M@ v@K@ �  ��      @K@ x@K@ �  ��      @J@ z@K@ �  ��      @J@ }@K@ �  ��      @SK @K@ � � ��      @�@K@ �  ��      @�@K@ �