# Calibration operations by filter.
#
# X - crosstalk correction
# O - overscan fitting and subtraction
# T - trimming 
# Z - zero (bias) subtraction
# D - dark subtraction
# E - flat field (prefer sky flat)
# F - flat field (prefer dome flat)
# W - wcs solution 
# S - saturation fixing and masking
# B - bleed trail fixing and masking 
# P - bad pixel fixing and masking
# A - fringe removal
# H - dark sky flat
# G - pupil ghost


# Common values.
CLASS = "'calops'"
DATATYPE = "'keyword'"

FILTER,VALUE

\'bokr','XOTZFHWSBPA'
\'g','XOTZFHWSBP'
\'r','XOTZFHWSBPA'
\'Ha+4nm','XOTZFHWSBP'
