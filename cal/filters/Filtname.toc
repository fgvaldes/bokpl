# Mapping from filter keyword to pipeline names, typically the short name.
# This is also used to correct for typos in the keyword.

# Common values.
CLASS = "'filtname'"
DATATYPE = "'keyword'"

FILTER,VALUE

#\'none','none'
\'bokr','r'
\'g','g'
\'r','r'
\'Ha+4nm','ha'
