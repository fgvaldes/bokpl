# Wavelength bandpass ranges for filters.
#
# The value contains the central wavelength, the FWHM, the units,
# and the source.

# Common values.
CLASS = "'filtwave'"
DATATYPE = "'keyword'"

FILTER,VALUE

\'bokr','6292.28 1475.17 A kpno'
\'g','4750.58 1395.73 A guess'
\'Ha+4nm','6620.52 80.48 A kpno'
\'r','6292.28 1475.17 A kpno'
