procedure topREQ (input)

file	input			{prompt="Input list of exposures"}
int	status = 1		{prompt="Return status"}
struct	*fd

begin

	string	in, in0, obstype, caldat, dateobs, timeobs
	real	mjd, t

	task $mjd2date = "$!mjd2date"

	fd = input
	while (fscan (fd, in) != EOF) {
	    in = substr (in, strldx("/",in)+1, 999)
	    in0 = in // "[0]"

	    # Set DTCALDAT
	    hedit (in0, "DTCALDAT", "(DATE)", add+, show+)

	    # Set OBSTYPE
	    hselect (in0, "imagetyp", yes) | scan (obstype)
	    hedit (in0, "OBSTYPE", obstype, add+, show+)

	    # Set MJD
	    hselect (in0, "JULIAN,TIME-OBS", yes) | scan (mjd,t)
	    mjd = mjd - 2400000.5 + t / 24.
	    hedit (in0, "MJD-OBS", mjd, add+, show+)

	    # Set DATEOBS and OBSID
	    hselect (in0, "DATE-OBS,TIME-OBS", yes) | scan (dateobs, timeobs)
	    dateobs = dateobs//"T"//timeobs
	    hedit (in0, "DATE-OBS", dateobs)
	    dateobs = substr (dateobs, 3, strldx(".",dateobs)-1)
	    hedit (in0, "OBSID", "bok23."//dateobs)

	    # Set NSA name
	    print (dateobs) | translit ("STDIN", "-:", del+) |
		translit ("STDIN", "T", "_") | scan (dateobs)
	    dateobs = "ksb_" // dateobs
	    if (obstype == "zero")
		dateobs += "_zri.fits.fz"
	    else if (obstype == "flat")
		dateobs += "_fri.fits.fz"
	    else if (obstype == "dark")
		dateobs += "_dri.fits.fz"
	    else
		dateobs += "_ori.fits.fz"
	    ;

	    hedit (in0, "FILENAME", dateobs, add+, show+)
	    hedit (in0, "DTNSANAM", dateobs, add+, show+)
	    hedit (in0, "DTACQNAM", in, add+, show+)
	    hedit (in0, "PROCTYPE", "raw", add+, show+)
	    hedit (in0, "PRODTYPE", "Image", add+, show+)

	    # Rename
	    rename (in, dateobs)
	    print (dateobs, >> "topspecial.tmp")
	    in = dateobs; in0 = in // "[0]"
	}
	fd = ""

	rename ("topspecial.tmp", "topstage.tmp")

	status = 1
end
