REPLACE INTO PSQ VALUES (
'DCPTEST', 'DCPTEST', 'DCPTESTD', 'DCP', 'dcp', 'enabled',
'instrument=''mosaic_2'' and filter similar to ''[ugriz]%'''
);

REPLACE INTO DCPTEST (dataset) VALUES ('20040909');
REPLACE INTO DCPTEST (dataset) VALUES ('20050406');
REPLACE INTO DCPTEST (dataset) VALUES ('20060620');
REPLACE INTO DCPTEST (dataset) VALUES ('20060914');

REPLACE INTO DCPTESTD VALUES ('20040909', '20040909', '20040916',
  'start_date between timestamp ''2004-09-09'' and timestamp ''2004-09-16'' and dtpropid=''2004B-0208''');
REPLACE INTO DCPTESTD VALUES ('20050406', '20050406', '20050408',
  'start_date between timestamp ''2005-04-06'' and timestamp ''2005-04-08'' and dtpropid=''2005A-0566''');
REPLACE INTO DCPTESTD VALUES ('20060620', '20060620', '20060629',
  'start_date between timestamp ''2006-06-20'' and timestamp ''2006-06-29'' and dtpropid=''2006A-0086''');
REPLACE INTO DCPTESTD VALUES ('20060914', '20060914', '20060920',
  'start_date between timestamp ''2006-09-14'' and timestamp ''2006-09-20'' and dtpropid=''2006B-0116''');
