#!/usr/bin/python

import os
import cgi

from executeSQL import executeSQL

form = cgi.FieldStorage()

def printPrimary (user, queue, dataset, psqData):

    referer = os.environ ['HTTP_REFERER']
    comments = psqData ['comments']
    
    print 'Content-Type: text/html'
    print 
    print '<html>'
    print '  <head>'
    print '    <title></title>'
    print '  </head>'
    print '  <body>'
    print '    <h2>Add comments for dataset: %s_%s</h2>' % (queue, dataset)
    print '    <form action="psq_add_comments.cgi" method="post">'
    print '      <input name="user"    value="%s" type="hidden">' % (user)
    print '      <input name="queue"   value="%s" type="hidden">' % (queue)
    print '      <input name="dataset" value="%s" type="hidden">' % (dataset)
    print '      <input name="referer" value="%s" type="hidden">' % (referer)
    print '      <textarea name="comments" rows="1" cols="60" wrap="virtual">'
    if (comments != '&nbsp;'):
        print comments
    print '</textarea>'
    print '      <p>'
    print '        <input name="add" value="Add" type="submit">'
    print '        &nbsp;&nbsp;'
    print '        <input value="Reset" type="reset">'
    print '        &nbsp;&nbsp;'
    print '        <a href="%s" style="text-decoration:none">' % (referer)
    print '        <input value="Cancel" type="button"></a>'
    print '      </p>'
    print '    </form>'
    print '  </body>'
    print '</html>'
    
    return

if __name__ == "__main__":
    if (form.has_key ('user') and
        form.has_key ('queue') and
	form.has_key ('dataset')):

        user    = form.getvalue ('user')
        queue   = form.getvalue ('queue')
        dataset = form.getvalue ('dataset')

	sql = "SELECT comments FROM %s WHERE dataset='%s'" % (queue, dataset)
	(psqCols, psqData) = executeSQL (user, sql)
        printPrimary (user, queue, dataset, psqData[0])
    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
