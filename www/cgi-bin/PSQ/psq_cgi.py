from executeSQL import executeSQL

STATUS = {'completed' : 'black',
          'error'     : 'cyan',
          'hold'      : 'red',
          'lock'      : 'red',
          'nodata'    : 'purple',
          'pending'   : 'green',
          'submitted' : 'blue',
          'queued'    : 'blue',
          'resubmit'  : 'orange'}

STATE  = {'enabled'   : 'green',
          'disabled'  : 'red'}

PRIORITY = range (1, 6)

USE_COLOR = False

def psq_filter_cgi (user, update=False):
    
    print 'Content-Type: text/html'
    print
    print '<html>'
    print '  <head>'
    print '    <title>Pipeline Scheduling Queue Database</title>'
    print '  </head>'
    print '  <body>'
    print '    <h2>Select Pipeline Scheduling Queues</h2>'
    print '    <br />Note: % symbol can be used as a wildcard.'
    print '    <br />This form is to view the pipeline and its queues.'
    print '    <form action="./psq.cgi" method="post">'
    print '      <input name="user" value="%s" type="hidden">' % (user)
    print '      <table cellspacing="20">'
    print '        <tbody>'
    print '          <tr>'
    print '            <td>Target Pipeline (eg: %dir%)</td>'
    print '            <td><input name="pipeline" value="" width="30" /></td>'
    print '          </tr>'
    print '          <tr>'
    print '            <td>PSQ Name (eg: %K4M%)</td>'
    print '            <td><input name="psqname" value="" width="30" /></td>'
    print '          </tr>'
    print '          <tr>'
    print '            <td>State</td>'
    print '            <td>'
    print '              <select name="state" width="30" multiple="multiple" size="3" value="">'
    print '                <option selected="selected">All</option>'
    for state in STATE.keys ():
        print '                <option>%s</option>' % (state)
        pass
    print '              </select>'
    print '            </td>'
    print '          </tr>'
    print '        </tbody>'
    print '      </table>'
    print '      <p align="center">'
    print '        <input name="SUBMIT" value="Submit Query" type="submit" />'
    print '        &nbsp;&nbsp;'
    print '        <input name="RESET" value="Reset Query" type="reset" />'
    print '      </p>'
    print '    </form>'
    print '  </body>'
    print '</html>'
    
    return

def psq_cgi (vars, update=False):
    
    sql = "SELECT psqname,queue,data,application,pipeline,state FROM PSQ"
    sep = "WHERE"

    if vars.has_key ('user'):
        user = vars.getvalue ('user')
    else:
        user = "pipeline"
    if vars.has_key ('pipeline'):
        val = vars.getvalue ('pipeline')
	sql += " %s pipeline='%s'" % (sep, val)
    if vars.has_key ('psqname'):
        val = vars.getvalue ('psqname')
	sql += " %s psqname='%s'" % (sep, val)
	sep = "AND"
    if vars.has_key ('state'):
        val = vars.getvalue ('state')
	sql += " %s state='%s'" % (sep, val)
    sql += " ORDER BY state DESC,application,psqname"

    (psqCols, psqData) = executeSQL (user, sql)
    psq (user, psqCols, psqData, update)

    return


def psq (user, psqCols, psqData, update=False):

    state    = None
    nResults = len (psqData)

    print 'Content-Type: text/html'
    print
    print '<html>'
    print '  <head>'
    print '    <title>Pipeline Scheduling Queue Database</title>'
    print '  </head>'
    print '  <body>'
    state = './psq_queue_edit.cgi?user=%s&psqname=%s'
    print '    <h2>Pipeline Scheduling Queues</h2>'
    
    if nResults > 0:
        psqname = './psq_queue.cgi?user=%s&psqname=%s&queue=%s&data=%s'
        print '    <table border="1" cellpadding="3">'
        print '      <tbody>'
        print '        <tr style="background:teal;">'
        for column in psqCols:
	    if column == 'data':
	        pass
	    else:
		print '          <th>%s</th>' % (column)
		pass
            pass
        print '        </tr>'
        
        for result in psqData:
            if (USE_COLOR):
                print '        <tr style="background-color:%s">' % (STATE [result ['state']])
            else:
                print '        <tr>'
                pass
            for column in psqCols:
                if column == 'psqname':
                    query = psqname % (user, result ['psqname'], result ['queue'], result ['data'])
                    print '          <td align="center"><a href="%s">%s</a></td>' % (query, result [column])
                elif column == 'state':
                    if (update):
                        query = state % (user, result ['psqname'])
                        print '          <td align="center"><a href="%s" style="text-decoration:none"><button type="button"><font color="%s">%s</font></button></a></td>' % (query, STATE [result [column]], result [column])
                    else:
                        print '          <td align="center"><font color="%s">%s</font></td>' % (STATE [result [column]], result [column])
                        pass
                    pass
                elif column == 'data':
		    pass
                else:
                    print '          <td align="center">%s</td>' % (result [column])
                    pass
                pass
            print '        </tr>'
            pass
        print '      </tbody>'
        print '    </table>'
        pass
    else:
        print '    <div>Sorry, No Records Found in PSQ for the given selection criteria</div>'
        pass
    print '  </body>'
    print '</html>'
    
    return

def psq_queue_cgi (vars, update=False):
    
    if (vars.has_key ('user') and
        vars.has_key ('psqname') and
        vars.has_key ('queue') and
	vars.has_key ('data')):

	user    = vars.getvalue ('user')
        psqname = vars.getvalue ('psqname')
        queue   = vars.getvalue ('queue')
        data    = vars.getvalue ('data')
        
        sql = "SELECT dataset,status,submitted,completed,notified,start,end,query,subquery,comments"
	sql += " FROM PSQ,%s,%s" % (queue,data)
	sql += " WHERE queue='%s' AND dataset=name" % (queue)
        if (vars.has_key ('dataset')):
            val = vars.getvalue ('dataset')
            if (val):
                if (val.find ('%') >= 0):
                    sql += " AND dataset LIKE '%s'" % (val)
                else:
                    sql += " AND dataset='%s'" % (val)
                    pass
                pass
            pass
        if (vars.has_key ('priority')):
            val = vars.getvalue ('priority')
            if (val and not val == "All"):
                sql += " AND priority='%s'" % (val)
                pass
            pass
        if (vars.has_key ('status')):
            val = vars.getvalue ('status')
            if (val and not val == "All"):
                sql += " AND status='%s'" % (val)
                pass
            pass
        sql += " ORDER BY dataset;"

	(psqCols, psqData)  = executeSQL (user, sql)
	psq_queue (user, psqname, queue, data, psqCols, psqData, update)

    else:
        print 'Content-Type: text/html'
        print
        print 'ERROR: Missing required inputs: psqname, queue, and/or data'
        pass

    return

def psq_queue (user, psqname, queue, data, psqCols, psqData, update=False):

    print 'Content-Type: text/html'
    print
    print '<html>'
    print '  <head>'
    print '    <title>Pipeline Scheduling Queue Database</title>'
    print '  </head>'
    print '  <body>'
    print '    <h2>Datasets for Pipeline Scheduling Queue <font color="red">%s</font></h2>' % (psqname)
    print '<h4><a href="psq.cgi?user=%s">Back</a> to list of queues.</h3>' % (user)
    nRecords = len (psqData)
    if (nRecords > 0):
        print '    <table border="1" cellpadding="3">'
        print '      <tbody>'
        print '        <tr style="background:teal;">'
	print '          <th>Dataset</th>'
	print '          <th>Status</th>'
	print '          <th>Submitted</th>'
	print '          <th>Completed</th>'
	print '          <th>Notified</th>'
	print '          <th>Query</th>'
	print '          <th>Comments</th>'
	print '        </tr>'
	for result in psqData:
	    dataset = result ['dataset']
	    status = result ['status']
	    print '        <tr>'
	    print '          <td align="center">'
	    if (status == "pending"):
		if (result ['submitted'] == '&nbsp;'):
		    href = 'http://www.noao.edu/%s/forms/tel_sched/%s.html'
		    if ((queue[0] == 'C' or queue[0] == 'D') and dataset[0] == '2'):
		        href = href % ('ctio', dataset[0:6])
			href = '<a href="%s" target="schedule">%s</a>' % (href, dataset)
		    elif (queue[0] == 'K' and dataset[0] == '2'):
		        href = href % ('kpno', dataset[0:6])
			href = '<a href="%s" target="schedule">%s</a>' % (href, dataset)
		    else:
			href = dataset
			
		    print '            %s' % (href)
		else:
		    print '            <a href="./psq_plqueue.cgi?user=%s&queue=%s&dataset=%s">%s</a>' % (user, queue, dataset, dataset)
	    elif (status == "completed"):
		print '            <a href="./psq_plqueue.cgi?user=%s&queue=%s&dataset=%s">%s</a>' % (user, queue, dataset, dataset)
	    else:
		print '            <a href="./psq_plqueue.cgi?user=%s&queue=%s&dataset=%s">%s</a>' % (user, queue, dataset, dataset)
	    print '          </td>'
	    if (update):
		link = '<a href="./psq_status_edit.cgi?user=%s&dataset=%s&queue=%s&psqname=%s&data=%s" style="text-decoration:none">'
		link = link % (user, dataset, psqname, queue, data)
		button = '<button type="button"><font color="%s">%s</font></button>' % (STATUS [status], status)
		print '          <td align="center">%s%s</a></td>' % (link, button)
	    else:
		print '          <td align="center"><font color="%s">%s</font></td>' % (STATUS [status], status)
		pass
	    print '          <td align="center">%s</td>' % (result ['submitted'])
	    print '          <td align="center">%s</td>' % (result ['completed'])
	    #if (result ['notified'] == '&nbsp;' and
	    #    result ['status'] == 'completed' and
	    #    result ['completed'] != '&nbsp;'):
	    if (result ['notified'] == '&nbsp;' and
		result ['completed'] != '&nbsp;'):
		if (queue [:3] == 'C4M' or queue [:3] == 'DEC'):
		    instr = 'mosaic_2'
		elif (queue [:3] == 'K4M'):
		    instr = 'mosaic_1'
		else:
		    instr = 'unknown'
		href = '<a href="../notify.cgi?%s&%s_%s&%s&%s&%s">notify</a>' % (user, queue, dataset, result ['start'], result ['end'], instr)
		print '          <td align="center">%s</td>' % (href)
	    else:
		print '          <td align="center">%s</td>' % (result ['notified'])
	    print '          <td align="left">%s and %s</td>' % (result ['query'], result ['subquery'])
	    if (update and result ['comments'] == '&nbsp;'):
		link = '<a href="./psq_comments.cgi?user=%s&queue=%s&dataset=%s" style="text-decoration:none">'
		link = link % (user, queue, dataset)
		button = '<button type="button">%s</button>' % ('enter')
		print '          <td align="center">%s%s</a></td>' % (link, button)
	    elif (update):
		link = '<a href="./psq_comments.cgi?user=%s&queue=%s&dataset=%s">'
		link = link % (user, queue, dataset)
		print '          <td align="left">%s%s</a></td>' % (link, result['comments'])
	    else:
		print '          <td align="left">%s</td>' % (result ['comments'])
	    print '        </tr>'
	    pass
	print '      </tbody>'
	print '    </table>'
    else:
	print '    Sorry, No Records Found in %s for the given selection criteria' % (vars.getvalue ('queue'))
	pass
    print '  </body>'
    print '</html>'
    
    return

def psq_plqueue_cgi (vars, update=False):
    
    # Set user.
    if (vars.has_key ('user')):
	user = vars.getvalue ('user')
    else:
	user = "pipeline"

    # Create page with links outside of review?
    if (vars.has_key ('static')):
        static = vars.getvalue ('static')
    else:
        static = False

    # Get results for PLDATASETS.
    sql = "SELECT plqueue,pldataset,plsubset,plprocid,plsubmitted,plcompleted,plarchived,plcomments"
    sql += " FROM PLDATASETS WHERE plsubmitted IS NOT NULL"
    if (vars.has_key ('queue')):
	sql += " AND plqueue='%s'" % (vars.getvalue ('queue'))
    if (vars.has_key ('dataset')):
	sql += " AND pldataset='%s'" % (vars.getvalue ('dataset'))
    if (vars.has_key ('completed')):
	if (vars.getvalue ('completed') == 'yes'):
	    sql += " AND plcompleted IS NOT NULL"
	else:
	    sql += " AND plcompleted IS NULL"
    if (vars.has_key ('archived')):
	if (vars.getvalue ('archived') == 'yes'):
	    sql += " AND plarchived LIKE '2%'"
	elif (vars.getvalue ('archived') == 'no'):
	    #sql += " AND plcompleted IS NOT NULL AND plarchived IS NULL"
	    sql += " AND plcompleted IS NOT NULL AND plarchived NOT LIKE '2%'"
	else:
	    sql += " AND plarchived='%s'" % (vars.getvalue ('archived'))
    if (user == 'newfirm'):
        sql += " ORDER BY plsubset, plcompleted, plqueue, pldataset, plsubmitted"
    else:
        sql += " ORDER BY plcompleted, plqueue, pldataset, plsubset, plsubmitted"

    (psqCols, psqData)  = executeSQL (user, sql)
    psq_plqueue (user, psqCols, psqData, update, static)

    return

def psq_plqueue (user, psqCols, psqData, update=False, static=False):

    # Get pipeline applications for making links to review pages.
    sql = "SELECT queue,application FROM PSQ"
    (appCols, appData)  = executeSQL (user, sql)
    apps = {}
    for result in appData:
        apps [result ['queue']] = result ['application']

    # Create web page.
    print 'Content-Type: text/html'
    print
    print '<html>'
    print '  <head>'
    print '    <title>Pipeline Scheduling Queue Database</title>'
    if (not static):
	print '    <meta http-equiv="refresh" content="60">'
    print '  </head>'
    print '  <body>'
    print '    <h2>Pipeline Datasets</h2>'
    if (not static):
	print '<h4><a href="psq.cgi?user=%s">Back</a> to list of queues.</h3>' % (user)
    nRecords = len (psqData)
    if (nRecords > 0):
        print '    <table border="1" cellpadding="3">'
        print '      <tbody>'
        print '        <tr style="background:teal;">'
        for column in psqCols:
	    print '          <th>%s</th>' % (column)
        print '        </tr>'

        prevsubset = 'XxXxX'
        colors = [ 'ccffcc','aaeeaa','eeaaaa','cc8888']
        coloridx = 0
        
        for result in psqData:

	    queue = result ['plqueue']
	    app = apps [queue]
	    ds = "%s_%s" % (queue, result ['pldataset'])
	    ds1 = "%s_%s-%s" % (ds, result ['plprocid'], result ['plsubset'])
	    if (app == 'MOSAIC'):
		htmldir = "/%s/MarioData/MOSAIC/MOSAIC_DPS/output/html/%s" % (user, ds)
	    elif (app == 'NEWFIRM'):
		htmldir = "/%s/MarioData/NEWFIRM/NEWFIRM_NDP/output/html/%s" % (user, ds)

            if (user != 'newfirm'):
                print '        <tr>'
            else:
                if ( result ['plsubset'] != prevsubset ):
                    coloridx = not coloridx
                print '        <tr style=\"background-color:%s\">' % (colors[coloridx])
                prevsubset = result ['plsubset']
            for column in psqCols:
		if (column == 'plqueue'):
		    href = 'psq_queue.cgi?user=%s&psqname=%s&queue=%s&data=%sD' % \
		         (user, queue, queue, queue)
		    if (static):
			href = '%s' % (result [column])
		    else:
			href = '<a href="%s">%s</a>' % (href, result [column])
		    print '          <td align="center">%s</td>' % (href)
		elif (column == 'pldataset'):
		    if (static):
			href = '%s' % (result [column])
		    else:
			href = 'http://www.noao.edu/%s/forms/tel_sched/%s.html'
			if ((queue[0] == 'C' or queue[0] == 'D') and result['pldataset'][0] == '2'):
			    href = href % ('ctio', result['pldataset'][0:6])
			    href = '<a href="%s" target="schedule">%s</a>' % (href, result[column])
			elif (queue[0] == 'K' and result['pldataset'][0] == '2'):
			    href = href % ('kpno', result['pldataset'][0:6])
			    href = '<a href="%s" target="schedule">%s</a>' % (href, result [column])
			else:
			    href = result[column]
			
		    print '          <td align="center">%s</td>' % (href)
		elif (column == 'plsubset'):
                    
		    if (result ['plcompleted'] == '&nbsp;'):
			href = '%s' % (result [column])
		    elif (result ['plarchived'] == '&nbsp;'):
			href = '<a href="%s/%s.html" target="review1">%s</a>' % \
			    (htmldir, ds1, result [column])
		    else:
			href = '<a href="%s/%s.html" target="review2">%s</a>' % \
			    (htmldir, ds1, result [column])
		    print '          <td align="center">%s</td>' % (href)
		elif (not static and column == 'plarchived' and
		    result ['plcompleted'][0] != '2'):
		    print '          <td align="center">'
		    href = '<a href="../psqdelete.cgi?%s&%s&%s&%s&%s&%s">delete</a>' % \
		        (user,app, queue, result['pldataset'], result['plprocid'],
			result['plsubset'])
		    print '            %s' % (href)
		    button = '<button type="button">pars</button>'
		    href = '<a href="psq_actions_form.cgi?user=%s&app=%s&queue=%s&dataset=%s&procid=%s&subset=%s" style="text-decoration:none">%s</a>' % \
			   (user, app, queue, result['pldataset'],
			   result['plprocid'], result['plsubset'], button)
		    print '            &nbsp;&nbsp;&nbsp;&nbsp;%s' % (href)
		    print '          </td>'
		elif (not static and column == 'plarchived' and
		    result ['plarchived'] == '&nbsp;' and
		    result ['plcompleted'] != '&nbsp;'):
		    print '          <td align="center">'
		    if (user == 'newfirm'):
			href = '<a href="../stbsubmit.cgi?%s&%s&%s&%s&%s&%s">submit</a>' % \
			       (user,app, queue, result['pldataset'], result['plprocid'],
			    result['plsubset'])
			print '            %s' % (href)
			href = '<a href="../redo.cgi?%s&%s&%s&%s&%s&%s&redo">redo</a>' % \
			    (user,app, queue, result['pldataset'], result['plprocid'],
			    result['plsubset'])
			print '            %s' % (href)
		    else:
			href = '<a href="../stbsubmit.cgi?%s&%s&%s&%s&%s&%s">submit</a>' % \
			       (user,app, queue, result['pldataset'],
			       result['plprocid'], result['plsubset'])
			print '            %s' % (href)
		        button = '<button type="button">redo</button>'
			href = '<a href="psq_actions_form.cgi?user=%s&app=%s&queue=%s&dataset=%s&procid=%s&subset=%s" style="text-decoration:none">%s</a>' % \
			       (user, app, queue, result['pldataset'],
			       result['plprocid'], result['plsubset'], button)
			print '            &nbsp;&nbsp;&nbsp;&nbsp;%s' % (href)
		    print '          </td>'
		elif (not static and column == 'plcomments' and
		    result ['plcomments'] == '&nbsp;'):
		    link = '<a href="./psq_plcomments.cgi?user=%s&queue=%s&dataset=%s&procid=%s&subset=%s" style="text-decoration:none">'
		    link = link % (user, queue, result['pldataset'], result['plprocid'], result['plsubset'])
		    button = '<button type="button">%s</button>' % ('enter')
		    print '          <td align="center">%s%s</a></td>' % (link, button)
		elif (not static and column == 'plcomments'):
		    link = '<a href="./psq_plcomments.cgi?user=%s&queue=%s&dataset=%s&procid=%s&subset=%s">'
		    link = link % (user, queue, result['pldataset'], result['plprocid'], result['plsubset'])
		    print '          <td align="center">%s%s</a></td>' % (link, result [column])
		else:
		    print '          <td align="center">%s</td>' % (result [column])
            print '        </tr>'
	print '      </tbody>'
	print '    </table>'
    else:
	print '    Sorry, No Records Found'
    print '  </body>'
    print '</html>'
    
    return


def psq_data_cgi (vars, update=False):
    
    if (vars.has_key ('user') and
        vars.has_key ('dataset') and
        vars.has_key ('data')):
	
        user = vars.getvalue ('user')
        dataset = vars.getvalue ('dataset')
        data    = vars.getvalue ('data')
        
        sql = "SELECT start, end, subquery FROM %s WHERE name='%s';" % (data, dataset)
        (psqCols, psqData) = executeSQL (user, sql)
	psq_data (dataset, data, psqCols, psqData, update)

    else:
        print 'Content-Type: text/html'
        print
        print 'ERROR: Missing required inputs: dataset and/or data'
        pass

    return


def psq_data (dataset, data, psqCols, psqData, update):
        
    print 'Content-Type: text/html'
    print
    print '<html>'
    print '  <head>'
    print '    <title>Pipeline Scheduling Queue Database</title>'
    print '  </head>'
    print '  <body>'
    print '    <h2>Pipeline Scheduling Queue Dataset <font color="red"><span>%s</span></font></h2>' % (dataset)
    if (len (psqData) == 1):
	result = psqData [0]
	start  = result ['start']
	end    = result ['end']
	sub    = result ['subquery']
	print '    <table border="1" cellpadding="3">'
	print '      <tbody>'
        print '        <tr style="background:teal;">'
	print '          <th>Dataset</th>'
	print '          <th>Start Date</th>'
	print '          <th>End Date</th>'
	print '          <th>SubQuery</th>'
	print '        </tr>'
	print '        <tr>'
	print '          <td align="center">%s</td>' % (dataset)
	print '          <td align="center">%s</td>' % (start)
	print '          <td align="center">%s</td>' % (end)
	print '          <td align="center">%s</td>' % (sub)
	print '        </tr>'
	print '      </tbody>'
	print '    </table>'
    elif (len (psqData) > 1):
	print '    Sorry, too many (%s) records returned!' % (len (psqData))
    else:
	print '    Sorry, No Records Found in %s for the given selection criteria' % (data)
	pass
    print '  </body>'
    print '</html>'
    
    return
