#!/usr/bin/python

import cgi

from executeSQL   import executeSQL
from psq_cgi import psq

form = cgi.FieldStorage()

if __name__ == "__main__":
    if (form.has_key ('user') and
        form.has_key ('psqname') and
	form.has_key ('state')):

	user = form.getvalue ('user')
        psqname = form.getvalue ('psqname')
        state   = form.getvalue ('state')
        
        # Execute the SQL UPDATE statement:
        sql = "UPDATE PSQ SET state='%s' WHERE psqname='%s'"
        executeSQL (user, sql % (state, psqname))

	sql = "SELECT psqname,queue,data,application,pipeline,state FROM PSQ"
	sql += " ORDER BY state DESC,application,psqname"
	(psqCols, psqData) = executeSQL (user, sql)
        psq (user, psqCols, psqData, True)
    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
