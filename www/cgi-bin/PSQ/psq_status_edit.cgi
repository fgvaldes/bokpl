#!/usr/bin/python

import os
import cgi
import warnings
warnings.filterwarnings ('ignore')

from executeSQL import executeSQL

form = cgi.FieldStorage()

def printPrimary (user, psqname, dataset, queue, data, psqData):

    referer = os.environ ['HTTP_REFERER']
    
    print 'Content-Type: text/html'
    print 
    print '<html>'
    print '  <head>'
    print '    <title></title>'
    print '  </head>'
    print '  <body>'
    #print '    <h2>Dataset: %s %s</h2>' % (queue, dataset)
    print '    <form action="edit_psq_status.cgi" method="post">'
    print '      <input name="user" value="%s" type="hidden">' % (user)
    print '      <input name="dataset" value="%s" type="hidden">' % (dataset)
    print '      <input name="queue"   value="%s" type="hidden">' % (queue)
    print '      <input name="psqname" value="%s" type="hidden">' % (psqname)
    print '      <input name="data"    value="%s" type="hidden">' % (data)
    print '      <input name="referer" value="%s" type="hidden">' % (referer)
    print '      <table cellspacing="20">'
    print '        <tbody>'
    print '          <tr>'
    print '            <th>Queue</th>'
    print '            <td>%s</td>' % (queue)
    print '          </tr>'
    print '          <tr>'
    print '            <th>Dataset</th>'
    print '            <td>%s</td>' % (dataset)
    print '          </tr>'
    print '          <tr>'
    print '            <th>Priority</th>'
    print '            <td>'
    print '              <select name="priority">'
    for x in [1,2,3,4,5]:
        if (int (psqData ['priority']) == x):
            print '                <option selected="selected">%s</option>' % (x)
        else:
            print '                <option>%s</option>' % (x)
            pass
        pass
    print '              </select>'
    print '            </td>'
    print '          </tr>'
    print '          <tr>'
    print '            <th>Status</th>'
    print '            <td>'
    print '              <select name="status">'
    for x in ['completed', 'error', 'hold', 'pending', 'submitted', 'resubmit',
        'nodata', 'lock']:
        if (psqData ['status'] == x):
            print '                <option selected="selected">%s</option>' % (x)
        else:
            print '                <option>%s</option>' % (x)
            pass
        pass
    print '              </select>'
    print '            </td>'
    print '          </tr>'
    print '          <tr>'
    print '            <th>Submitted</th>'
    print '            <td align="center">'
    print '              <span>%s</span>' % (psqData ['submitted'])
    print '            </td>'
    print '          </tr>'
    print '          <tr>'
    print '            <th>Completed</th>'
    print '            <td align="center">'
    print '              <span>%s</span>' % (psqData ['completed'])
    print '            </td>'
    print '          </tr>'
    print '        </tbody>'
    print '      </table>'
    #print '      <p align="center">'
    print '      <p>'
    print '        <input name="action" value="Stage Dataset" type="submit">'
    print '        &nbsp;&nbsp;'
    print '        <input name="action" value="Submit Dataset" type="submit">'
    print '        &nbsp;&nbsp;'
    print '        <input name="action" value="Update Record" type="submit">'
    print '        &nbsp;&nbsp;'
    print '        <input name="action" value="Reset" type="reset">'
    print '      </p>'
    print '    </form>'
    print '  </body>'
    print '</html>'
    
    return

if __name__ == "__main__":
    if (form.has_key ('user') and
        form.has_key ('psqname') and
	form.has_key ('dataset') and
	form.has_key ('queue') and
	form.has_key ('data')):

	user    = form.getvalue ('user')
        psqname = form.getvalue ('psqname')
        dataset = form.getvalue ('dataset')
        queue   = form.getvalue ('queue')
        data    = form.getvalue ('data')
        sql     = "SELECT * FROM %s, %s WHERE name=dataset AND dataset='%s';" % (data, queue, dataset)
        (psqCols, psqData) = executeSQL (user, sql)
        printPrimary (user, psqname, dataset, queue, data, psqData [0])
    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
