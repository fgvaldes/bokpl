#!/usr/bin/python
#
# This is basically a translator from POST to environment variables.

import os
import cgi

form = cgi.FieldStorage()

if __name__ == "__main__":

    if (form.has_key ('user')):
	os.environ ['PSQ_USER']      = '%s' % (form.getvalue ('user'))
    if (form.has_key ('queue')):
	os.environ ['PSQ_QUEUE']     = '%s' % (form.getvalue ('queue'))
    if (form.has_key ('app')):
	os.environ ['PSQ_APP']       = '%s' % (form.getvalue ('app'))
    if (form.has_key ('dataset')):
	os.environ ['PSQ_DATASET']   = '%s' % (form.getvalue ('dataset'))
    if (form.has_key ('procid')):
	os.environ ['PSQ_PROCID']    = '%s' % (form.getvalue ('procid'))
    if (form.has_key ('subset')):
	os.environ ['PSQ_SUBSET']    = '%s' % (form.getvalue ('subset'))
    if (form.has_key ('pgr')):
	os.environ ['PSQ_PGR']       = '%s' % (form.getvalue ('pgr'))
    if (form.has_key ('frg')):
	os.environ ['PSQ_FRG']       = '%s' % (form.getvalue ('frg'))
    if (form.has_key ('sft')):
	os.environ ['PSQ_SFT']       = '%s' % (form.getvalue ('sft'))
    if (form.has_key ('sft_smooth')):
	os.environ ['PSQ_SFTSMOOTH'] = '%s' % (form.getvalue ('sft_smooth'))
    if (form.has_key ('sft_projsmooth')):
	os.environ ['PSQ_SFTPROJ'] = '%s' % (form.getvalue ('sft_projsmooth'))
    if (form.has_key ('referer')):
	os.environ ['HTTP_REFERER']  = '%s' % (form.getvalue ('referer'))

    # Execute command.
    action = form.getvalue ('action')
    os.system ('../redo.cgi %s' % action) 

