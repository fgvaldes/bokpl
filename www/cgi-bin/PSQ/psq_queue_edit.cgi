#!/usr/bin/python

import cgi

from executeSQL import executeSQL

form = cgi.FieldStorage()

def printPrimary (user, data):
    
    print 'Content-Type: text/html'
    print 
    print '<html>'
    print '  <head>'
    print '    <title></title>'
    print '  </head>'
    print '  <body>'
    print '    <h2>Updating Queue %s</h2>' % (data ['psqname'])
    print '    <form action="edit_psq_queue.cgi" method="post">'
    print '      <input name="user" value="%s" type="hidden">' % (user)
    print '      <input name="psqname" value="%s" type="hidden">' % (data['psqname'])
    print '      <table cellspacing="20">'
    print '        <tbody>'
    print '          <tr>'
    print '            <th>PSQ Name</th>'
    print '            <td>%s</td>' % (data ['psqname'])
    print '          </tr>'
    print '          <tr>'
    print '            <th>Queue</th>'
    print '            <td>%s</td>' % (data ['queue'])
    print '          </tr>'
    print '          <tr>'
    print '            <th>Data</th>'
    print '            <td>%s</td>' % (data ['data'])
    print '          </tr>'
    print '          <tr>'
    print '            <th>Pipeline</th>'
    print '            <td>%s</td>' % (data ['pipeline'])
    print '          </tr>'
    print '          <tr>'
    print '            <th>State</th>'
    print '            <td>'
    print '              <select name="state">'
    if (data ['state'] == "enabled"):
        print '                <option selected="selected">enabled</option>'
    else:
        print '                <option>enabled</option>'
        pass
    if (data ['state'] == "disabled"):
        print '                <option selected="selected">disabled</option>'
    else:
        print '                <option>disabled</option>'
        pass
    print '              </select>'
    print '            </td>'
    print '          </tr>'
    print '          <tr>'
    print '            <th>Query</th>'
    print '            <td>%s</td>' % (data ['query'])
    print '          </tr>'
    print '        </tbody>'
    print '      </table>'
    print '      <p align="center">'
    print '        <input name="SUBMIT" value="Update Record" type="submit">'
    print '        &nbsp;&nbsp;'
    print '        <input name="RESET" value="Reset" type="reset">'
    print '      </p>'
    print '    </form>'
    print '  </body>'
    print '</html>'
    
    return

if __name__ == "__main__":
    if (form.has_key ('user') and form.has_key ('psqname')):

	user = form.getvalue ('user')
        psqname = form.getvalue ('psqname')

        (columns, results) = executeSQL (user, "SELECT * FROM PSQ where psqname='%s';" % (psqname))
        printPrimary (user, results [0])
    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
