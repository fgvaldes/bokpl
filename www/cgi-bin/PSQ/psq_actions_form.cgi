#!/usr/bin/python

import os
import cgi

form = cgi.FieldStorage()

def printPrimary (user, app, queue, dataset, procid, subset):
    
    referer = os.environ ['HTTP_REFERER']

    print 'Content-Type: text/html'
    print 
    print '<html><head><title></title></head><body>'
    print '<h2>%s_%s_%s-%s</h2>' % (queue, dataset, procid, subset)
    print '<form action="psq_redo.cgi" method="post">'
    print '  <input name="user"    value="%s" type="hidden">' % (user)
    print '  <input name="app"     value="%s" type="hidden">' % (app)
    print '  <input name="queue"   value="%s" type="hidden">' % (queue)
    print '  <input name="dataset" value="%s" type="hidden">' % (dataset)
    print '  <input name="procid"  value="%s" type="hidden">' % (procid)
    print '  <input name="subset"  value="%s" type="hidden">' % (subset)
    print '  <input name="referer" value="%s" type="hidden">' % (referer)

    print '  <p><table cellspacing="5">'
    print '  <tr><th><th>&nbsp;default&nbsp;<th>&nbsp;yes&nbsp;<th>&nbsp;no&nbsp;<th>&nbsp;lib first&nbsp;<th>&nbsp;lib only'
    print '  <tr>'
    print '  <th align="right">Pupil Removal:'
    print '  <td align="center"><input type="radio" name="pgr" value="default" checked>'
    print '  <td align="center"><input type="radio" name="pgr" value="yes">'
    print '  <td align="center"><input type="radio" name="pgr" value="no">'
    print '  <td align="center"><input type="radio" name="pgr" value="libfirst">'
    print '  <td align="center"><input type="radio" name="pgr" value="libonly 0">'
    print '  <tr>'
    print '  <th align="right">Fringe Subtraction:'
    print '  <td align="center"><input type="radio" name="frg" value="default" checked>'
    print '  <td align="center"><input type="radio" name="frg" value="yes">'
    print '  <td align="center"><input type="radio" name="frg" value="no">'
    print '  <td align="center"><input type="radio" name="frg" value="libfirst">'
    print '  <td align="center"><input type="radio" name="frg" value="libonly 0">'
    print '  <tr>'
    print '  <th align="right">Dark Sky Flat:'
    print '  <td align="center"><input type="radio" name="sft" value="default" checked>'
    print '  <td align="center"><input type="radio" name="sft" value="yes">'
    print '  <td align="center"><input type="radio" name="sft" value="no">'
    print '  <td align="center"><input type="radio" name="sft" value="libfirst">'
    print '  <td align="center"><input type="radio" name="sft" value="libonly 0">'

    print '  <tr>'
    print '  <tr><th><th>&nbsp;default&nbsp;<th>&nbsp;yes&nbsp;<th>&nbsp;no&nbsp;<th>&nbsp;constant&nbsp;'
    print '  <tr>'
    print '  <th align="right">Sky Flat Smooth:'
    print '  <td align="center"><input type="radio" name="sft_smooth" value="default" checked>'
    print '  <td align="center"><input type="radio" name="sft_smooth" value="yes">'
    print '  <td align="center"><input type="radio" name="sft_smooth" value="no">'
    print '  <td align="center"><input type="radio" name="sft_smooth" value="constant">'
    print '  <tr>'
    print '  <tr><th><th>&nbsp;default&nbsp;<th>&nbsp;no&nbsp;'
    print '  <tr>'
    print '  <th align="right">Sky Flat Projection Smoothing:'
    print '  <td align="center"><input type="radio" name="sft_projsmooth" value="default" checked>'
    print '  <td align="center"><input type="radio" name="sft_projsmooth" value="no">'
    print '  <tr>'
    print '  </table>'

    print '  <p><a href="%s" style="text-decoration:none">' % (referer)
    print '  <input value="Cancel" type="button"></a>'
    print '  <input name="action" value="Pars" type="submit">'
    print '  <input name="action" value="Redo" type="submit">'
    print '</form>'
    print '</body></html>'
    
    return

if __name__ == "__main__":
    if (form.has_key ('user') and
        form.has_key ('app') and
	form.has_key ('queue') and
	form.has_key ('dataset') and
	form.has_key ('procid') and
	form.has_key ('subset')):

	user    = form.getvalue ('user')
        app     = form.getvalue ('app')
        queue   = form.getvalue ('queue')
        dataset = form.getvalue ('dataset')
        procid  = form.getvalue ('procid')
        subset  = form.getvalue ('subset')
        printPrimary (user, app, queue, dataset, procid, subset)
    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
