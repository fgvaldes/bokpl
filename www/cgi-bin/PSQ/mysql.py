import popen2
from string import split, strip

def executeSQL (user, sql):
    
    """
    Returns the output of the SQL as a list of columns and a dictionary.
    The columns are only needed if the order is important.
    """

    cmd   = """/usr/bin/mysql --xml --user=%s -pMosaic_DHS %s -e "%s" | sed -n 's:^\W*::p' | sed -e 's: xsi\:nil="true" />:>NULL</field>:' | sed -e :a -e '/field>$/N; s/\\n//; ta' | grep field | sed -n 's:field.name="::pg' | sed -n 's:">:,|,:pg' | sed -n 's:</field>row>::p'"""
    cmd   = cmd % (user, user, sql)
    o,i,e = popen2.popen3 (cmd)

    cols = []
    data  = []
    lines = o.readlines ()
    o.close ()
    i.close ()
    e.close ()

    if (lines):
	for field in split (lines[0].strip (), '</field>'):
	    cols.append (split (field, ',|,', 1) [0])
        for line in lines:
            row = {}
            for field in split (line.strip (), '</field>'):
                (col, val) = split (field, ',|,', 1)
		if (val == "NULL"):
		    val = "&nbsp;"
                row [col] = val
            data.append (row)
    return (cols, data)
