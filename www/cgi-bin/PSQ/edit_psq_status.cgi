#!/usr/bin/python

import os
import cgi
import time

from executeSQL   import executeSQL
from psq_cgi import psq_queue

form = cgi.FieldStorage()

if __name__ == "__main__":
    if (form.has_key ('user')     and
        form.has_key ('psqname')  and
        form.has_key ('queue')    and
        form.has_key ('data')     and
        form.has_key ('dataset')  and
        form.has_key ('priority') and
        form.has_key ('status')   and
	form.has_key ('action')):
        
	user     = form.getvalue ('user')
        psqname  = form.getvalue ('psqname')
        queue    = form.getvalue ('queue')
        data     = form.getvalue ('data')
        dataset  = form.getvalue ('dataset')
        priority = form.getvalue ('priority')
        status   = form.getvalue ('status')
        action   = form.getvalue ('action')
        referer  = form.getvalue ('referer')

	if (action == 'Update Record'):
	    # Execute the SQL UPDATE statement:
	    sql = "UPDATE %s SET priority=%s, status='%s' WHERE dataset='%s'"
	    executeSQL (user, sql % (queue, priority, status, dataset))

	    print 'Location: %s\n' % (referer)
	elif (action == 'Stage Dataset'):
            os.environ ['HTTP_REFERER'] = referer
	    os.system ('../psa.cgi %s %s %s %s' % ('stage', user, queue, dataset))
	else:
            os.environ ['HTTP_REFERER'] = referer
	    os.system ('../psa.cgi %s %s %s %s' % ('submit', user, queue, dataset))
    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
