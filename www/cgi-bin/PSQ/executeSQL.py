import popen2
from string import split, strip

def executeSQL (user, sql):
    
    """
    Returns the output of the SQL as a list of columns and a dictionary.
    The columns are only needed if the order is important.
    """

    cols = []
    data  = []

    if user in ['deccp']:
	db = 'psqdb.' + user
	cmd = """/usr/bin/sqlite3 -csv -header -nullvalue 'NULL' %s "%s" """
	cmd   = cmd % (db, sql)
	o,i,e = popen2.popen3 (cmd)

	lines = o.readlines ()
	o.close ()
	i.close ()
	e.close ()

	if (lines):
	    cols = None
	    for line in lines:
		line = line.strip ()
		if not cols:
		    cols = line.split (',')
		else:
		    row = {}
		    vals = line.split (',')
		    for i, val in enumerate (vals):
			if val == 'NULL':
			    val = '&nbsp;'
			row[cols[i]] = val
		    data.append (row)

    elif user in ['pipeline', 'newfirm']:
	cmd   = """/usr/bin/mysql --xml --user=%s -pMosaic_DHS %s -e "%s" | sed -n 's:^\W*::p' | sed -e 's: xsi\:nil="true" />:>NULL</field>:' | sed -e :a -e '/field>$/N; s/\\n//; ta' | grep field | sed -n 's:field.name="::pg' | sed -n 's:">:,|,:pg' | sed -n 's:</field>row>::p'"""
	cmd   = cmd % (user, user, sql)
	o,i,e = popen2.popen3 (cmd)
	lines = o.readlines ()
	o.close ()
	i.close ()
	e.close ()

	if (lines):
	    for field in split (lines[0].strip (), '</field>'):
		cols.append (split (field, ',|,', 1) [0])
	    for line in lines:
		row = {}
		for field in split (line.strip (), '</field>'):
		    (col, val) = split (field, ',|,', 1)
		    if (val == "NULL"):
			val = "&nbsp;"
		    row [col] = val
		data.append (row)

    else:
	cmd   = """/usr/bin/mysql --xml --user=%s -pMosaic_DHS %s -e "%s" | sed -n 's:^\W*::p' | sed -e 's: xsi\:nil="true" />:>NULL</field>:' | sed -e :a -e '/field>$/N; s/\\n//; ta' | grep field | sed -n 's:field.name="::pg' | sed -n 's:">:,|,:pg' | sed -n 's:</field>row>::p'"""
	cmd   = cmd % (user, user, sql)
	o,i,e = popen2.popen3 (cmd)
	lines = o.readlines ()
	o.close ()
	i.close ()
	e.close ()

	if (lines):
	    for field in split (lines[0].strip (), '</field>'):
		cols.append (split (field, ',|,', 1) [0])
	    for line in lines:
		row = {}
		for field in split (line.strip (), '</field>'):
		    (col, val) = split (field, ',|,', 1)
		    if (val == "NULL"):
			val = "&nbsp;"
		    row [col] = val
		data.append (row)

    return (cols, data)
