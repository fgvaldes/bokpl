#!/usr/bin/python

import os
import cgi
from executeSQL   import executeSQL
from psq_cgi import psq_queue

form = cgi.FieldStorage()

if __name__ == "__main__":
    if (form.has_key ('user') and
        form.has_key ('queue') and
        form.has_key ('dataset') and
	form.has_key ('referer')):
        
        user     = form.getvalue ('user')
        queue    = form.getvalue ('queue')
        dataset  = form.getvalue ('dataset')
        referer  = form.getvalue ('referer')

	if (form.has_key ('comments')):
	    comments = form.getvalue ('comments')
	else:
	    comments = ''

	comments = comments.replace ('\r','')
	comments = comments.replace ('\n',' ')
	comments = comments.strip ()

	sql = "UPDATE %s" % queue
	if (comments == ''):
	    sql += " SET comments=NULL"
	else:
	    sql += " SET comments='%s'" % comments
	sql += " WHERE dataset='%s'" % dataset

	executeSQL (user, sql)

	print 'Location: %s\n' % (referer)

    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
