#!/usr/bin/python

import os
import cgi
from executeSQL   import executeSQL
from psq_cgi import psq_queue

form = cgi.FieldStorage()

if __name__ == "__main__":
    if (form.has_key ('user') and
        form.has_key ('queue') and
        form.has_key ('dataset') and
        form.has_key ('procid') and
        form.has_key ('subset') and
	form.has_key ('referer')):
        
        user     = form.getvalue ('user')
        queue    = form.getvalue ('queue')
        dataset  = form.getvalue ('dataset')
        procid   = form.getvalue ('procid')
        subset   = form.getvalue ('subset')
        referer  = form.getvalue ('referer')

	if (form.has_key ('comments')):
	    comments = form.getvalue ('comments')
	else:
	    comments = ''

	comments = comments.replace ('\r','')
	comments = comments.replace ('\n',' ')
	comments = comments.strip ()

	sql = "UPDATE PLDATASETS"
	if (comments == ''):
	    sql += " SET plcomments=NULL"
	else:
	    sql += " SET plcomments='%s'" % comments
	sql += " WHERE plqueue='%s'" % queue
	sql += " AND pldataset='%s'" % dataset
	sql += " AND plprocid='%s'" % procid
	sql += " AND plsubset RLIKE '%s *$'" % subset

	executeSQL (user, sql)

	print 'Location: %s\n' % (referer)

    else:
        print 'Content-Type: text/html\n\nERROR!'
        pass
    pass
