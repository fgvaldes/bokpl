#!/bin/csh -f

# The is not completely general.
umask 000
setenv USER $2

switch ($USER)
case deccp:
    setenv NHPPS /shared/deccp/NHPPS

    # PSA is an NHPPS python utility.
    setenv PATH $NHPPS/bin:$PATH
    setenv PYTHONPATH $NHPPS/extern/site-packages:$NHPPS/src/python
    setenv PYTHONUNBUFFERED yes

    setenv NHPPS_NODE_PS $HOST
    setenv NHPPS_PORT_PS 7013
    setenv NHPPS_PORT_NM 7011
    breaksw
endsw

#printf "Content-type: text/plain\n\n"
#echo $*
#env
#exit

if ($1 == "submit") then
    psa --queue=$3 --dataset=$4
else if ($1 == "stage") then
    psa --queue=$3 --dataset=$4 --result=stage
endif

printf "Location: $HTTP_REFERER\n\n"
