#!/bin/env perl

require "cgi-helpers.pl";
umask 002;

# Print the CGI response header
&openhtml( "Processing results" );
# Get the CGI variables into a list of strings
%cgivars= &getcgivars;
# Set up the environment
&getenvironment;

print "Processing selections ...<br>\n";

$dataset = $cgivars{'dataset'};
$stbdir = $ENV{'NEWFIRM_STB'};
$dtsdir = $ENV{'NEWFIRM_DTS'};
$setredo = 0;
$user = $ENV{'USER'};

while (($key,$value)=each(%cgivars)) {
    print "$key $value<br>";
    # Find keys that start with "ac-", which contain the requested action
    if ( $key =~ /^ac-(.*)/ ) {
	$seqid = $1; $oseqid = $seqid;
	$seqid =~ s/\-//;
	if ( $value eq "submit" ) {
	    # Format the trigger correctly
	    $trigger = $stbdir."/input/".$dataset;
	    if ( $oseqid=~ /^\d{4}$/ ) {
		$trigger .= "-drk".$oseqid;
	    } elsif ( $oseqid =~ /-/ ) {
		@fields = split( "-", $oseqid );
		$trigger .= "-fil".$fields[0]."-flt".$fields[1];
	    } else {
		$trigger .= "-gos".$oseqid;
	    }
	    # Set the entry for this dataset to pending
	    system( "psqpltable $dataset-$oseqid archived pending" );
	    # Submit this dataset by creating the trigger file
	    system( "touch ".$trigger."-stb.stbtrig" );
	} elsif ( $value eq "redo" ) {
	    # Setup redo of this dataset by renaming .enids file
	    $thisdataset = $dataset."-gos".$seqid;
	    $enids = $dtsdir."/output/".$thisdataset."-dts/".$thisdataset.".enids";
	    if ( -e $enids ) {

		@fields = split( "_", $dataset );
		$plpat = "^".$seqid." *\$";
		$cmd .= <<EOF;
UPDATE $fields[0] SET status='resubmit'
WHERE dataset='$fields[1]' && status='submitted';
UPDATE PLDATASETS SET plarchived='redo'
WHERE plarchived IS NULL
AND plqueue='$fields[0]' AND pldataset='$fields[1]'
AND plprocid='$fields[2]' AND plsubset RLIKE '$plpat';
EOF
                open(TMPFILE,">/tmp/dataproc.tmp");
		print TMPFILE $cmd;
		close(TMPFILE);
		system( "setenv USER $user ; /shared/$user/V1.0/MarioSrc/bin/psqdb < /tmp/dataproc.tmp" );
		unlink( "/tmp/dataproc.tmp" );

		rename( $enids, $enids."BAK" );
		$setredo = 1;

		# Configure datasets that are to be reprocessed based on
		# the option specified by the user. First, determine the
		# selected options. 
		$mthd = "mthd-".$oseqid;
		$othr = "othr-".$oseqid;
		$skysub = $cgivars{$mthd};
		$other = $cgivars{$othr};
		# Check whether dataset-specific configuration file already
		# exists. If so, read it, except for parameters that will
		# be overwritten.
		# Check whether there are any requested changed
		$changes = 0;
		(($skysub ne "non")||($other ne "default")) && ($changes = 1);
		if ( $changes ) {
		    $configfile = "/shared/$user/V1.0/config/".$fields[1]."-gos".$seqid.".cl";
		    undef( @config );
		    # Check whether dataset-specific config file already exists.
		    # If so, read the contents and delete it.
		    if ( -e $configfile ) {
			open( CONFIG, $configfile );
			while (<CONFIG>) {
			    chomp;
			    next if ((/sps_resbck/) && ($skysub ne "non"));
			    next if ((/ring_removal/) && ($other ne "default"));
			    next if ((/stripe_removal/) && ($other ne "default"));
			    push( @config, $_ );
			}
			close(CONFIG);
			unlink( $configfile );
		    }
		    # Add the changed content. 
		    # For residual sky subtraction, the configuration string consists
		    # off three parts, one for in-field dithereing, followed by
		    # one for 4Q mode, and ending in the one for offset sky data.
		    # The ace and polyN methods can be applied to all data, i.e.,
		    # data taken in 4Q or offset mode can use those methods. However,
		    # it is not possible (at the moment) to apply the 4Q or offset
		    # method to data taken in other modes (e.g., 4Q cannot be
		    # applied to in-field dithering or offset sky). If 4Q or offset
		    # is selected, the other parameters will be left at reasonable
		    # default values (the same as those in config/NEWFIRM.cl at the 
		    # time of this writing). This is fine as long as 4Q or offset
		    # is selected for data taken in 4Q or offset mode. On the other 
		    # hand, if 4Q is selected for data taken in in-field dither
		    # mode, the data will NOT be processed in 4Q mode. Instead,
		    # the default provided below will be used, and this value
		    # could be different from what is provided in other configuration
		    # files.
		    $res = "sps_resbck = \"";
		    if ( $skysub eq "4q" ) {
			$res .= "poly5,4Q,none";
		    } elsif ( $skysub eq "ace" ) {
			$res .= "ace,ace,ace";
		    } elsif ( $skysub eq "off" ) {
			$res .= "poly5,poly2,none";
		    } elsif ( $skysub eq "pol2" ) {
			$res .= "poly2,poly2,poly2";
		    } elsif ( $skysub eq "pol5" ) {
			$res .= "poly5,poly5,poly5";
		    } else {
			$res .= "poly5,poly2,none";
		    }
		    $res .= "\"";
		    if ( $skysub ne "non" ) {
			push( @config, $res );
		    }
		    # The assumption is that the default is to have sky and ring
		    # subtraction disabled. Thus, only enabling is possible here.
		    # If the user selects "default", the entries are not changed,
		    # and hence will be left at previously selected value.
		    if ( $other ne "default" ) {
			$rings = "no"; $stripe = "smart";
			( $other =~ /rings/ ) && ( $rings = "yes" );
			( $other =~ /stripe/ ) && ( $stripe = "yes" );
			( $other =~ /nostripe/ ) && ( $stripe = "no" );
			push( @config, "stripe_removal = \"".$stripe."\"" );
			push( @config, "ring_removal = ".$rings );
		    }
		    # Write the config file (back) to disk
		    open( CONFIG, ">".$configfile );
		    for($i=0;$i<=$#config;$i++) {
			print CONFIG $config[$i]."\n";
		    }
		    close( CONFIG );
		}
	    }
	}
    }
}

# Update overview page
$output = `setenv USER $user ; /shared/$user/V1.0/MarioSrc/pipelines/BIN/mkndpreview --plqueue $dataset`;
#print "$output\n";

# Load the overview page
print "<script type=\"text/javascript\">\n";
print "window.location.href=\"".$ENV{HTTP_REFERER}."\"\n";
print "</script>\n";

# Print close of HTML file
print "</body>\n</html>\n" ;

exit;
