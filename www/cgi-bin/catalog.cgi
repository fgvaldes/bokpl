#!/bin/csh -f

set query = `echo $QUERY_STRING | tr "&" " "`

# The is not yet general.
umask 002
setenv USER $query[1]
setenv PATH /shared/$USER/V1.0/MarioSrc/bin:/shared/$USER/V1.0/bin.redhat:$PATH
setenv LD_LIBRARY_PATH /shared/$USER/V1.0/bin.redhat
setenv NHPPS_SYS_NAME $query[2]
setenv DMData ../html/$USER/DMData
set dataset = ${query[3]}_${query[4]}_${query[5]}-ftr${query[6]}
set dir = ../html/$USER/$NHPPS_SYS_NAME/${NHPPS_SYS_NAME}_DTS/output/${dataset}-dts
set config = /shared/$USER/V1.0/config/ConfigMap.list
set enids = $dir/${dataset}.enids

printf "Content-type: text/plain\n\n"
#echo $query
#env
#echo $dataset
#echo $dir
#echo $enids
#exit

set dataset = ${query[3]}_${query[4]}_${query[5]}-${query[6]}
calquality $NHPPS_SYS_NAME -q 2. ${dataset}'*skymap.fits'
