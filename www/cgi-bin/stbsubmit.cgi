#!/bin/csh -f

set query = `echo $QUERY_STRING | tr "&" " "`

# The is not yet general.
umask 002
setenv USER $query[1]
setenv PATH /shared/$USER/V1.0/MarioSrc/bin:$PATH

switch ($USER)
    case pipeline:
        set dataset = ${query[3]}_${query[4]}_${query[5]}-${query[6]}
        switch ($HOST)
	case pipedmn:
	    set trigdir = "/data2/MarioData/MOSAIC/MOSAIC_STB/input"
	case pipefs3:
	    set trigdir = "/data1/pipeline/MarioData/MOSAIC/MOSAIC_STB/input"
	    breaksw
	endsw
	breaksw
    case newfirm:
	set dataset = ${query[3]}_${query[4]}_${query[5]}-${query[6]}
	set trigdir = "/data1/newfirm/MarioData/NEWFIRM/NEWFIRM_STB/input"
	breaksw
    default:
	echo Content-Type: text/html
	echo
	echo "<html><head><title>WARNING</title></head>"
	echo "<body>Please configure stbsubmit.cgi for user "$USER
	echo "</body></html>"
	exit 0
	break
endsw

#printf "Content-type: text/plain\n\n"
#env
#echo $dataset
#echo $trigdir
#exit

# Update database.
psqpltable $dataset archived pending

# Submit trigger file. Note that this requires that apache has write
# privileges in the $USER account. This should be set up by adding $USER
# to the apache group, and apache to the iraf group (it is not clear whether
# both of these are necessary). Also, make sure that all directories
# are group-writable. Finally, it may be necessary to restart the httpd
# daemon.
switch ($USER)
    case pipeline:
	set fields = `echo $dataset | tr "-" " "`
	touch ${trigdir}/$fields[1]-ftr$fields[2]-stb.stbtrig
	breaksw
    case newfirm:
	set fields = `echo $dataset | tr "-" " "`
	touch ${trigdir}/$fields[1]-gos$fields[2]-stb.stbtrig
	breaksw
    default:
	echo Content-Type: text/html
	echo
	echo "<html><head><title>WARNING</title></head>"
	echo "<body>Please configure trigger file in stbsubmit.cgi for user "$USER
	echo "</body></html>"
	exit 0
	break
endsw


set fields = `echo $dataset | tr "-" " "`

printf "Location: $HTTP_REFERER\n\n"
