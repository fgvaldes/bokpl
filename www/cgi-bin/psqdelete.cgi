#!/bin/csh -f

#printf "Content-type: text/plain\n\n"

set query = `echo $QUERY_STRING | tr "&" " "`

# The is not yet general.
umask 002
setenv USER $query[1]
setenv PATH /shared/$USER/V1.0/MarioSrc/bin:/shared/$USER/V1.0/bin.redhat:$PATH
setenv LD_LIBRARY_PATH /shared/$USER/V1.0/bin.redhat

# In order to support filters which differ only by case the plsubset
# field is stored as binary and we need to use a pattern to match
# whitespace at the end of the field.

set plpat = '^'${query[6]}' *$'

# Update database.
psqdb << EOF
DELETE FROM PLDATASETS
WHERE plqueue='$query[3]' AND pldataset='$query[4]'
AND plprocid='$query[5]' AND plsubset RLIKE '$plpat';
EOF

printf "Location: $HTTP_REFERER\n\n"
