#!/bin/csh -f

set query = `echo $QUERY_STRING | tr "&" " "`
setenv USER $query[1]
set dataset = $query[2]
set start = $query[3]
set end = $query[4]
set instr = $query[5]

setenv PATH /shared/$USER/V1.0/MarioSrc/bin:$PATH
if ($USER == "pipedev") then
    setenv NHPPS_SYS_NAME NEWFIRM
else
    setenv NHPPS_SYS_NAME MOSAIC
endif

#printf "Content-type: text/plain\n\n"
#echo notify.csh -n -q -s -l 3 -i $instr -d $start $end -b "'archive-notice@noao.edu mario_prog@noao.edu'"
#notify.csh -n -q -s -l 3 -i $instr -d $start $end -t 'valdes@noao.edu'
#echo psqpltable $dataset notified
#exit

# Run notify command.
notify.csh -n -q -s -l 3 -i $instr -d $start $end \
-b 'archive-notice@noao.edu mario_prog@noao.edu' \
>& /dev/null

# Update database.
psqpltable $dataset notified

printf "Location: $HTTP_REFERER\n\n"
