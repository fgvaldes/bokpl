#!/bin/csh -f
#
## ========
## pmon.cgi
## ========
## -----------------------------------
## Simple web browser pipeline monitor
## -----------------------------------
## 
## :Manual section: 1
## :Manual group: www/cgi
## 
## Usage
## -----
## 
## ``<url>/pmon.cgi[option]``
## 
##   | ?mosaic  - filter for higher level Mosaic pipelines
##   | ?<pipes> - filter entries for a specific pipeline(s)
##   | ?err     - filter for non-standard flags
## 
## Description
## -----------
## 
## This simple pipeline monitor depends on the file ``pmdata.dat``, which is
## simply the output of osf_test.  This file is refreshed by running
## ``pmdata.csh`` or ``serverpm``.  The latter updates the file
## periodically.  Typical usage is to run ``serverpm [poll_time]``
## as the pipeline user with a relatively low demand poll time
## (1min-5min).  Then when needed remotely where ``pipemon`` is not
## appropriate select this pipeline monitor from a browser bookmark.
## 
## The output may be filtered using the first argument.  The ``mosaic``
## option selects the pipelines which ARE NOT below:
## 
## ngt|day|xtc|xtm|scl|sif|spg|sfg|ssf|srs|ctr|str|sci|mov|mdp|sdt
## 
## The ``err`` option selects entries which have a flag not in the set
## ``pcwnd_``.
##
## Otherwise the specified pattern is match to the pipeline name.
## This is most used as a list of '|' delimited pipelines.
## 
## This implementation has a hardwired path to the pmdata.dat file.
## 
## Examples
## --------
## 
##   | http://pipedmn/cgi-bin/pmon.cgi
##   | http://pipedmn/cgi-bin/pmon.cgi?mosaic
##   | http://pipedmn/cgi-bin/pmon.cgi?dts|sdt
##   | http://pipedmn/cgi-bin/pmon.cgi?err

set query = `echo $QUERY_STRING | tr "&" " "`

printf "Refresh: 60\n"
printf "Content-type: text/plain\n\n"

printf "Simple NHPPS Pipeline Monitor\n\n"

foreach line (`cat ../html/pipeline/MarioHome/pmdata.dat | sort -k 1.37`)
set words = (`echo $line | tr ":." " "`)
set dataset = `echo $words[3] | sed 's+___*$++'`
set node = `echo $words[6] | tr -d "_"`

if ( $#query == 0 ) then

printf "%-20s %-4s %-12s %s\n" $dataset $words[4] $node $words[2]
else

switch ($query[1])
case err:
echo $words[2] | egrep -q '^[pcwnd_]*$'
if ($status) then
printf "%-20s %-4s %-12s %s\n" $dataset $words[4] $node $words[2]
endif
breaksw
case mosaic:
echo $words[2] | egrep -qv 'd_*$'
if ($status) continue
echo $words[2] | egrep -q '^[pcwnd_]*$'
if ($status) then
printf "%-20s %-4s %-12s %s\n" $dataset $words[4] $node $words[2]
else
echo $words[4] | egrep -q 'ftr|ngt|day|xtc|xtm|scl|sif|spg|sfg|ssf|srs|ctr|str|sci|mov|mdp|sdt'
if ($status) then
printf "%-20s %-4s %-12s %s\n" $dataset $words[4] $node $words[2]
endif
endif
breaksw
default:
echo $words[4] | egrep -qv $query[1]
if ($status) then
printf "%-20s %-4s %-12s %s\n" $dataset $words[4] $node $words[2]
endif
endsw

endif

end
