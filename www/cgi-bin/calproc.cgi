#!/bin/env perl

require "cgi-helpers.pl";

# Print the CGI response header
&openhtml ( "Command execution results" );
# Get the CGI variables into a list of strings
%cgivars= &getcgivars;
# Set up the environment
&getenvironment;

unless ( exists $cgivars{ 'cmd' } ) {
    print "ERROR: no command specified!" ;
    return ;
}

$dataset = $cgivars{'dataset'};
$outdata = "/tmp/caldel_$dataset";
@ids = split( ",", $cgivars{'calibration'} );
( -e $outdata ) && ( unlink $outdata );
open( OUTFILE, ">$outdata" );
for($i=0;$i<=$#ids;$i++) {
    print OUTFILE "$dataset-$ids[$i]\n";
    print "$dataset-$ids[$i]<br>\n";
}
close( OUTFILE );
print "<hr>\n";

@cmds = split /,\s*/, $cgivars{ 'cmd' } ;

# Execute the command(s)
foreach( @cmds ) {
    print "Command:<pre>$_</pre>" ;
    print "Output:<pre>" ;
    $ret = system( $_ ) >> 8 ;
    print "</pre>\n" ;
    print "Returned:<pre>$ret</pre><br>\n" ;
}

# Print close of HTML file
print "</body>\n</html>\n" ;

exit ;
