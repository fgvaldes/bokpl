#!/bin/env perl

require "cgi-helpers.pl";
umask 002;

# Print the CGI response header
&openhtml( "Processing results" );
# Get the CGI variables into a list of strings
%cgivars= &getcgivars;
# Set up the environment
&getenvironment;

print "Updating information ... please wait ...<br>\n";

$dataset = $cgivars{'dataset'};
$user = $ENV{'USER'};

# Update overview page
$output = `/shared/$user/V1.0/MarioSrc/pipelines/BIN/mkndpreview --plqueue $dataset`;
#print "$output\n";

# Load the overview page
print "<script type=\"text/javascript\">\n";
print "window.location.href=\"".$ENV{HTTP_REFERER}."\"\n";
print "</script>\n";

# Print close of HTML file
print "</body>\n</html>\n" ;

exit;
