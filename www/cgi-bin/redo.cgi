#!/bin/csh -f

# This script requires a directory $USER in /www/cgi-bin/. The contents
# of /www/cgi-bin/$USER/ should be a symbolic link to MarioData and 
# DMData for that user.

# It also requires that catalog.db be writable by the world in order for
# calquality to update the database!

set action = "$1"

# The is not yet general.
umask 000
setenv USER $PSQ_USER
setenv pipeshared /shared/$USER/V1.0
setenv NHPPS $pipeshared/lib/nhpps
setenv PATH $NHPPS/bin:$pipeshared/MarioSrc/bin:$pipeshared/bin.redhat:$PATH
setenv LD_LIBRARY_PATH /shared/$USER/V1.0/bin.redhat
setenv MarioData ../../html/$USER/MarioData
setenv DMData ../../html/$USER/DMData
setenv NHPPS_SYS_NAME $PSQ_APP
# Set dataset:
switch ($USER)
    case pipeline:
        set dataset = ${PSQ_QUEUE}_${PSQ_DATASET}_${PSQ_PROCID}-ftr${PSQ_SUBSET}
	breaksw
    case newfirm:
        set dataset = ${PSQ_QUEUE}_${PSQ_DATASET}_${PSQ_PROCID}-gos${PSQ_SUBSET}
	breaksw
    default:
	echo Content-Type: text/html
	echo
	echo "<html><head><title>WARNING</title></head>"
	echo "<body>Please configure redo.cgi for user "$USER
	echo "</body></html>"
	exit 0
	break
endsw
set shortname = ${PSQ_QUEUE}_${PSQ_DATASET}_${PSQ_PROCID}-${PSQ_SUBSET}
set dir = $MarioData/$NHPPS_SYS_NAME/${NHPPS_SYS_NAME}_DTS/output/${dataset}-dts
set PipeConfig = /shared/$USER/V1.0/config
set ConfigMap = $PipeConfig/ConfigMap.list
set enids = $dir/${dataset}.enids

#printf "Content-type: text/plain\n\n"
#echo $PSQ_SFTPROJ
#echo redo.cgi $action
#env | grep PSQ_
#echo $dataset
#echo $dir
#echo $enids
#exit

if ( -e $enids || $action == "Pars" ) then

# Process the parameters.
set pat = ${PSQ_QUEUE}_${PSQ_DATASET}_'?*'-ftr${PSQ_SUBSET}
set config = ${PipeConfig}/${PSQ_QUEUE}_${PSQ_DATASET}-${PSQ_SUBSET}

echo $PSQ_PGR $PSQ_FRG $PSQ_SFT | grep -q lib
if (! $status) then
    printf 'cal_dmjd = 365\n' >> ${config}.tmp
endif

if ("$PSQ_PGR" != "default") then
    printf 'pgr_cal = "%s"\n' "$PSQ_PGR" >> ${config}.tmp
    calquality -a $NHPPS_SYS_NAME -c pupil -q -1. ${shortname}'*pgr.fits' > /dev/null
endif
if ("$PSQ_FRG" != "default") then
    printf 'frg_cal = "%s"\n' "$PSQ_FRG" >> ${config}.tmp
    calquality -a $NHPPS_SYS_NAME -c fringe -q -1. ${shortname}'*frg.fits' > /dev/null
endif
if ("$PSQ_SFT" != "default") then
    printf 'sft_cal = "%s"\n' "$PSQ_SFT" >> ${config}.tmp
    calquality -a $NHPPS_SYS_NAME -c sft -q -1. ${shortname}'*sft.fits' > /dev/null
endif
if ($PSQ_SFTSMOOTH != "default") then
    if ($PSQ_SFTSMOOTH == "constant") then
	printf 'sft_smooth = "yes"\n' >> ${config}.tmp
	printf 'sft_func = "cheb"\n' >> ${config}.tmp
	printf 'sft_xorder = 1\n' >> ${config}.tmp
	printf 'sft_yorder = 1\n' >> ${config}.tmp
    else
	printf 'sft_smooth = "%s"\n' $PSQ_SFTSMOOTH >> ${config}.tmp
    endif
    calquality -a $NHPPS_SYS_NAME -c sft -q -1. ${shortname}'*sft.fits' > /dev/null
endif
if ($PSQ_SFTPROJ != "default") then
    if ($PSQ_SFTPROJ == "no") printf 'sft_projsmooth = 0\n' >> ${config}.tmp
    calquality -a $NHPPS_SYS_NAME -c sft -q -1. ${shortname}'*sft.fits' > /dev/null
endif

# Note that if all the default values are used it will not undo any previous
# configuration.  It would have to be done manually.

if (-e ${config}.tmp) then

# Set configuration file and add to ConfigMap.
mv ${config}.tmp ${config}.cl
grep -q `basename ${config}.cl` $ConfigMap
if ($status) then
    printf '%s	%s\n' "$pat" `basename ${config}.cl` >> $ConfigMap
endif

endif

# Setup the redo by updating the PSQ and eliminating the enids file.

if ($action == "Redo") then

# In order to support filters which differ only by case the plsubset
# field is stored as binary and we need to use a pattern to match
# whitespace at the end of the field.

set plpat = '^'${PSQ_SUBSET}' *$'

# Update database.
psqdb << EOF
UPDATE $PSQ_QUEUE SET status='resubmit'
WHERE dataset='$PSQ_DATASET' && status='submitted';
UPDATE PLDATASETS SET plarchived='redo'
WHERE plarchived IS NULL
AND plqueue='$PSQ_QUEUE' AND pldataset='$PSQ_DATASET'
AND plprocid='$PSQ_PROCID' AND plsubset RLIKE '$plpat';
EOF

# Move the enids file to cause a redo when the dataset is submitted.
mv $enids ${enids}BAK

endif


endif

printf "Location: $HTTP_REFERER\n\n"
