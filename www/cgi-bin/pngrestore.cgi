#!/bin/csh -f

# This script requires a directory $USER in /www/cgi-bin/. The contents
# of /www/cgi-bin/$USER/ should be a symbolic link to MarioData and 
# DMData for that user.

set query = `echo $QUERY_STRING | tr "&" " "`

# The is not yet general.
umask 002
setenv USER $query[1]
setenv PATH /shared/$USER/V1.0/MarioSrc/bin:/shared/$USER/V1.0/bin.redhat:$PATH
setenv LD_LIBRARY_PATH /shared/$USER/V1.0/bin.redhat
setenv MarioData ../html/$USER/MarioData
setenv NHPPS_SYS_NAME $query[2]
set png = $query[3]
set dir = $NHPPS_SYS_NAME/${NHPPS_SYS_NAME}_DPS/output/html/png

#printf "Content-type: text/plain\n\n"
#echo $query
#env
#echo pngrestore -r $MarioData/$dir/$png
#exit

if (! -e $MarioData/$dir/$png) then
    #printf "Content-type: text/plain\n\n"
    #printf "Fetching from archive: %s\n" $png
    cd ..
    setenv MarioData $cwd/html/$USER/MarioData
    cd /tmp
    pngrestore -r $MarioData/$dir/$png > /dev/null
endif

#printf "Content-type: text/html\n\n"
#printf "<html><head><title>%s</title></head><body>\n" $png
#printf '<IMG SRC="../%s" WIDTH="1000">\n' $USER/MarioData/$dir/$png
#printf "</body></html>\n"
if (-e $MarioData/$dir/$png) then
    printf "Content-type: image/png\n\n"
    cat $MarioData/$dir/$png
else
    printf "Content-type: text/plain\n\n"
    echo Failed to retrieve $png
