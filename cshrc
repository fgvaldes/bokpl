# Set IRAF environment.
source $iraf/unix/hlib/irafuser.csh
set -f path = ($x11iraf/bin.${IRAFARCH} $path $hlib $hbin)
set -f cdpath = ($iraf/extern $cdpath)
unsetenv hlib hbin tmp
alias cl $iraf/bin.${IRAFARCH}/ecl.e

# Set NHPPS environment.
source	$NHPPS/config/Setup.csh

# Set operator configuration directory.
setenv PipeConfig $plroot/PipeConfig

# Set NHPPS_DB.
setenv NHPPS_DB $PipeConfig/nhpps.db

# Set PSQDB.
setenv NHPPS_PSQDB $plroot/PSQDB/psqdb.$plname

# Set IQS mode.
setenv IQS DECam
setenv PGUSER pipeline
setenv PGDATABASE metadata
setenv PGHOST db.sdm.noao.edu
setenv PGPORT 5432
setenv PGPASSWORD   Mosaic_DHS

# Set mkpkg.
alias mk$plname "mkpkg \!:* bin=$plroot/${plshort}BIN xml=$plroot/src/xml cal=$NHPPS_PIPECAL"
setenv PKGENV "plapp nfextern"
setenv plapp $NHPPS_PIPEAPPSRC/plapp/
setenv nfextern $irafextern/nfextern/

# Paths
set -f path = ($plroot/${plshort}BIN $iraf/x11iraf/bin${arch} $shared/miscbin /shared/cfitsio/bin $path)
set -l cdpath = ($shared $cdpath)
