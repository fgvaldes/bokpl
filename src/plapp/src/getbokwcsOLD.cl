# GETBOKWCS -- Get a BOK WCS indexed by FOCUSVAL.

procedure getbokwcs (image)

file	image			{prompt="Image"}
file	wcsdb = ""		{prompt="WCS database"}
string	focusval = ""		{prompt="Image focus values"}
string	best = ""		{prompt="Best available focus"}
real	dmin = INDEF		{prompt="Distance"}
int	nfound = 0		{prompt="Number of WCS databases"}

struct	*fd

begin
	real	f1, f2, f3, a, b, c, d
	string	im
	file	focvals
	struct	focval

	# Get parameters.
	im = image

	# Get the image focus values.
	hselect (im, "$FOCUSVAL", yes) |
	    translit ("STDIN", "*", " ") | scan (focval)
	if (fscan (focval, f1, f2, f3) == 3) {
	    focusval = substr (focval, 2, 999)

	    # Get list of available focus values.
	    getcal ("wcsdb", image=im, mjd="!MJD-OBS", filter="!FILTER",
		match="focusvals", path+)
	    if (getcal.nfound < 1)
		getcal ("wcsdb", image=im, mjd="!MJD-OBS", match="focusvals",
		    path+)
	    if (getcal.nfound < 1) {
		getcal ("wcsdb", image=im, mjd="!MJD-OBS", match="focusvals",
		    path+, sql="STDOUT")
		error (1, "FOCUSVAL file not found")
	    }
	    focvals = getcal.value

	    # Find the closest match.
	    fd = focvals
	    while (fscan (fd, focval) != EOF) {
		if (fscan (focval, a, b, c) != 3)
		    next

		d = max (abs(f1-a), abs(f2-b), abs(f3-c))
		if (isindef(dmin)) {
		    dmin = d
		    best = focval
		} else if (d < dmin) {
		    dmin = d
		    best = focval
		}
	    }
	    fd = ""
	} else
	    best = "default"

	# Get WCS database for closest match.
	getcal ("wcsdb", image=im, mjd="!MJD-OBS", filter="!FILTER",
	    match=best, path+)
	if (getcal.nfound < 1)
	    getcal ("wcsdb", image=im, mjd="!MJD-OBS", match=best, path+)
	nfound = getcal.nfound
	wcsdb = getcal.value
end
