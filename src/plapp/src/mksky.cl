# MKSKY -- Make exposure ACE sky images (maybe sky or sigma).

procedure mksky (input, output)

file	input			{prompt="File of sky images"}
file	output			{prompt="Output sky image"}
real	blank = 0.		{prompt="Blank value"}
bool	verbose = no		{prompt="Verbose?"}

begin
	file	in, out, im
	int	i, j, k, i1, i2, j1, j2, nc, nl
	string	sec

	in = input
	out = output

	k = 0
	for (j=1; j<=2; j+=1) {
	    for (i=1; i<=2; i+=1) {
	        k += 1
		match ("-"//k//"_", in) | scan (im)
		if (nscan() == 0)
		    next
		if (!imaccess(out)) {
		    mkglbhdr ("@"//in, "mkskyhdr", ref="", exclude="")
		    hselect (im, "NAXIS1,NAXIS2", yes) | scan (nc, nl)
		    i2 = nc * 2; j2 = nl * 2
		    mkpattern (out, pattern="constant", option="replace",
			v1=blank, size=1, pixtype="real", ndim=2,
			ncols=i2, nlines=j2, header="mkskyhdr")
		    imdelete ("mkskyhdr")
		}
		i1 = (i - 1) * nc + 1; i2 = i * nc
		j1 = (j - 1) * nl + 1; j2 = j * nl
		printf ("%s[%d:%d,%d:%d]\n", out, i1, i2, j1, j2) | scan (sec)
		imcopy (im, sec, verbose=verbose) 
	    }
	}
end
