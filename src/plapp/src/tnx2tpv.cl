

	# Delete any old PV keywords.
	hedit (im, "PV?_*", del+)

	# Pull out the WAT1 (RA axis) keywords.  Make sure they are sorted
	# and then extract the string value in file tmp1
	imhead (im, l+) | match ("^WAT1_") | sort |
	    fields ("STDIN", 2, > tmp2)

	# Concatenate all the strings into one long string in file tmp1.
	# Finish with a newline.
	fd = tmp2
	while (fscan (fd, line) != EOF)
	    printf ("%s", substr(line,1,68), >> tmp1)
	fd = ""; delete (tmp2)
	printf ("\n", >> tmp1)

	# Separate long string into words and skip the first 12.
	words (tmp1) | words | fields ("STDIN", 1, lines="13-", > tmp1)

# In my script the xxorder and yyorder are set in the conversion to TNX.
# For the BASS data products the orders are the second and third coefficients
# and have values of 5.  In the code below only the xxorder is needed to know
# when to increment to the next y power. So:

xxorder = 5

	# Scan through the file figuring out the indices for the PV keywords.
	# Note that the first coefficient needs to have 1 added otherwise
	# the TNX coefficent values are the same as TPV.
	fd = tmp1; m = 0; n = 0; i = 0
	while (fscan (fd, x) != EOF) {
	    j = m + n
	    if (j == 1) {
		if (m == 1) x += 1
		i = 1 + n
	    } else if (j == 2)
		i = 4 + n
	    else if (j == 3)
		i = 7 + n
	    else if (j == 4)
		i = 12 + n
	    else if (j == 5)
		i = 17 + n
	    else if (j == 6)
		i = 24 + n
	    else if (j == 7)
		i = 31 + n
	    #printf ("C%d%d = PV1_%d = %g\n", m, n, i, x) # For dumping

	    # Format the keyword and add it to the header.
	    printf ("PV1_%d\n", i) | scan (key)
	    hedit (im, key, x, add+)
	    m += 1
	    if (m == xxorder - n) {
		m = 0; n += 1
	    }
	}
	fd = ""; delete (tmp1)

	# Repeat for the WAT2 keywords (DEC axis).
	imhead (im, l+) | match ("^WAT2_") | sort |
	    fields ("STDIN", 2, > tmp2)
	fd = tmp2
	while (fscan (fd, line) != EOF)
	    printf ("%s", substr(line,1,68), >> tmp1)
	fd = ""; delete (tmp2)
	printf ("\n", >> tmp1)
	words (tmp1) | words | fields ("STDIN", 1, lines="13-", > tmp1)
	fd = tmp1; m = 0; n = 0; i = 0
	while (fscan (fd, x) != EOF) {
	    j = m + n
	    if (j == 1) {
		if (n == 1) x += 1
		i = 1 + m
	    } else if (j == 2)
		i = 4 + m
	    else if (j == 3)
		i = 7 + m
	    else if (j == 4)
		i = 12 + m
	    else if (j == 5)
		i = 17 + m
	    else if (j == 6)
		i = 24 + m
	    else if (j == 7)
		i = 31 + m
	    #printf ("C%d%d = PV2_%d = %g\n", m, n, i, x)
	    printf ("PV2_%d\n", i) | scan (key)
	    hedit (im, key, x, add+)
	    m += 1
	    if (m == xxorder - n) {
		m = 0; n += 1
	    }
	}
	fd = ""; delete (tmp1)

	# Add the CTYPE keywords.
	ctype1 = substr(ctype1, 1, 5) // "TPV"
	ctype2 = substr(ctype2, 1, 5) // "TPV"
	hedit (im, "CTYPE1", ctype1)
	hedit (im, "CTYPE2", ctype2)

	# Get rid of the WAT keywords.
	hedit (im, "WAT*", del+)
