file	in

for (i=1; i<=4; i+=1) {
    files ("*-"//i//"_sft.fits") | scan (in)
    s1 = substr (in, 1, strldx("-",in)-1)

    for (j=1; j<=4; j+=1) {
        if (j == 1)
	    s3 = "[1:2016,1:2048]"
	else if (j == 2)
	    s3 = "[4032:2017,1:2048]"
	else if (j == 3)
	    s3 = "[1:2016,4096:2049]"
	else if (j == 4)
	    s3 = "[4032:2017,4096:2049]"

	k = (i - 1) * 4 + j
	printf ("%sI-%02d.fits\n", s1, k) | scan (s2)
	imcopy (in//s3, s2)
	hedit (s2, "AMPNUM", k, add+)
	hedit (s2, "CCDSEC", s3, add+)
	hedit (s2, "CCDMEAN", 1., add+)
	hedit (s2, "MKILLUM", "Done", add+)
	putcal ("dillcor", s2, dir="!dtcaldat", imageid=k, filter="!filter",
	    ncombine="!ncombine", quality=0, mjd="!mjd-obs", match="!noccbin",
	    action="")
    }
}
