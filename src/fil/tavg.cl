procedure tavg (table, col1, col2, bin)

file	table		{prompt="Input table"}
string	col1		{prompt="X column"}
string	col2		{prompt="Y column"}
int	bin		{prompt="Bin size"}
int	off = 31	{prompt="Offset for binning"}

begin
	file	t
	string	c1, c2, c3, expr
	int	b, i, i1, i2
	real	x, y

	t = table
	c1 = col1
	c2 = col2
	b = bin
	c3 = c1 // "," // c2

	printf ("int((%s+%d)/%d)\n", c1, off, b) | scan (expr)
	tcalc (t, "bin", expr)
	tstat (t, "bin", out="")
	i1 = int (tstat.vmin)
	i2 = int (tstat.vmax)

	for (i = i1; i <= i2; i += 1) {
	    expr = "bin==" // i
	    tquery (t, "tavg.tmp", expr, c3, "")
	    tstat ("tavg.tmp", c2, out="")
	    x = b * (i + 0.5)
	    y = tstat.mean
	    print (x, y)
	    delete ("tavg.tmp")
	}
end
