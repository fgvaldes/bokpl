#!/bin/env pipecl
#
# FILPLOT - Make plots as in filzpt.  This is used for after the fact
# plots on the saved phtcats.

file	phtcat, out, mc

# Packages and tasks.
plot
task	tavg = NHPPS_PIPEAPPSRC$/fil/tavg.cl

if (fscan(args,phtcat,out) < 2)
    logout
;

mc = out // ".mc"

tavg (phtcat, "x", "ddec", 64) |
    graph ( >G mc)
tavg (phtcat, "x", "dra", 64) |
    graph ( >>G mc)
tavg (phtcat, "y", "ddec", 64) |
    graph ( >>G mc)
tavg (phtcat, "y", "dra", 64) |
    graph ( >>G mc)

tselect (phtcat, out//"1_tmp.fits", "ccdnum==1")
tselect (phtcat, out//"2_tmp.fits", "ccdnum==2")
tselect (phtcat, out//"3_tmp.fits", "ccdnum==3")
tselect (phtcat, out//"4_tmp.fits", "ccdnum==4")

tavg (out//"3_tmp.fits", "x", "ddec", 64) | graph ( >>G mc)
tavg (out//"1_tmp.fits", "x", "ddec", 64) | graph ( >>G mc)
tavg (out//"2_tmp.fits", "x", "ddec", 64) | graph ( >>G mc)
tavg (out//"4_tmp.fits", "x", "ddec", 64) | graph ( >>G mc)

tavg (out//"2_tmp.fits", "y", "dra", 64) | graph ( >>G mc)
tavg (out//"1_tmp.fits", "y", "dra", 64) | graph ( >>G mc)
tavg (out//"3_tmp.fits", "y", "dra", 64) | graph ( >>G mc)
tavg (out//"4_tmp.fits", "y", "dra", 64) | graph ( >>G mc)

delete (out[1-4]_tmp.fits)

logout
