#!/bin/env pipecl
#
# FILDP - Collect the data products and make HTML review.

string	plqueue, plqname, plprocid, subset
string	ra, dec, dtnsanam, dateobs, exptime, propid, wcscal, magzero
int	ncombine
struct	title

# Get data products from the NSA and STK pipelines.
concat ("*.nsa") | match (".fz$", >> "fildp.tmp")
concat ("*stk") | match (".fits$", >> "fildp.tmp")
concat ("*.nsa,*stk") | match (".jpg$", >> "fildp.tmp")
concat ("*.nsa") | match ("hdr.txt$", >> "fildp.tmp")
concat ("*stk") | match ("stk.txt$", >> "fildp.tmp")
concat ("*stk") | match ("rej.txt$", >> "fildp.tmp")
move ("@fildp.tmp", '.', >& "dev$null")

# Set the PL keywords.
print (dataset.sname) | translit ("STDIN", "_-", " ") |
    scan (plqueue, plqname, plprocid, subset)

# Set the HTML preamble.
print ("index.html", >> dataset.fdelete)
title = dataset.sname
printf ('<html><title>%s</title></body><center><h1>%s</h1></center>\n',
    title, title, > "index.html")
s1 = dataset.sname // "_sky.html"
if (access(s1))
    printf ('<p><a href="%s">Sky Analysis</a>\n', s1, >> "index.html")
;
printf ('<table border="1">\n', >> "index.html")

##### Calibrations #####

files ("*_ci.fits.fz", > "fildp.tmp")
list = "fildp.tmp"
for (k=0; fscan(list,s1)!= EOF; k+=1) {
    i = strldx ("_", s1)
    s2 = substr (s1, 1, i-1)
    s3 = substr (s1, i-3, i-1)

    dtnsanam = s2 // "_ori"; ncombine = 1
    hselect (s1//"[0]", "DTNSANAM,$NCOMBINE", yes) | scan (dtnsanam,ncombine)
    i = strstr ("_ori", dtnsanam)
    if (s3 == "frt") {
        s3 = "Fringe Template"
	dtnsanam = substr (dtnsanam, 1, i) // "hci_" // subset // "_v1"
    } else if (s3 == "sft") {
        s3 = "Dark Sky Flat"
	dtnsanam = substr (dtnsanam, 1, i) // "ici_" // subset // "_v1"
    } else {
        s3 = "Unknown"
	dtnsanam = substr (dtnsanam, 1, i) // "uci_" // subset // "_v1"
    }

    if (mod (k, 3) == 0)
        printf ('<tr>', >> "index.html")
    ;
    printf ('<td>%s<br>%s<br>NCOMBINE %s\
	<td><a href="%s"><img src="%s"></a></tr>\n',
	s3, dtnsanam, ncombine, s2//"_cj2.jpg", s2//"_cj1.jpg", >> "index.html")
}
list = ""

##### Stacks #####

files ("*_si.fits", > "fildp.tmp")
list = "fildp.tmp"
for (k=0; fscan(list,s1)!= EOF; k+=1) {
    s2 = substr (s1, 1, strldx("_",s1)-1)
    s3 = s1

    hselect (s3, "DTNSANAM,DATE-OBS,$PROPID,$NCOMBINE,$EXPTIME", yes) |
	scan (dtnsanam,dateobs,propid,ncombine,x)
    i = strstr ("_ori", dtnsanam)
    if (i == 0)
        dtnsanam = s2 // "_si"
    else
	dtnsanam = substr (dtnsanam, 1, i) // "osi_" // subset // "_v1"

    printf ("%d\n", x) | scan (exptime)
    dateobs = substr (dateobs, 1, strldx(".",dateobs)-1)
    hselect (s3, "title", yes) | scan (title)
    if (stridx("'",title)>0)
	title = substr (title, stridx("'",title)+1, strldx("'",title)-1)
    ;

    if (mod (k, 3) == 0)
        printf ('<tr>', >> "index.html")
    ;
    printf ('<td><a href="%s">%s</a><br>%s<br>%d<br>%s<br>NCOMBINE %s\
	<br>%.20s', s2//"_hdr.txt", dtnsanam, dateobs, exptime, propid,
	ncombine, title, >> "index.html")
    printf ('<br><br><a href="%s">Selection Statistics</a>',
	s2//"_rej.txt", >> "index.html")
    printf ('<td><a href="%s"><img src="%s"></a>\n',
	s2//"_sg2.jpg", s2//"_sg1.jpg", >> "index.html")
}
list = ""

##### Objects #####

files ("*_oi.fits.fz", > "fildp.tmp")
list = "fildp.tmp"
for (k=0; fscan(list,s1)!=EOF; k+=1) {
    s2 = substr (s1, 1, strldx("_",s1)-1)
    s3 = s1 // "[0]"

    hselect (s3, "DTNSANAM,DATE-OBS,$PROPID,$EXPTIME,\
        $WCSCAL,$MAGZERO,TITLE", yes) | scan (dtnsanam,
	dateobs,propid,exptime,wcscal,magzero,title)
    i = strstr ("_ori", dtnsanam)
    if (i > 0)
	dtnsanam = substr (dtnsanam, 1, i) // "ooi_" // subset // "_v1"
    ;
    dateobs = substr (dateobs, 1, strldx(".",dateobs)-1)
    printf ("%d\n", real(exptime)+0.5) | scan (exptime)
    printf ("%.2f\n", real(magzero)) | scan (magzero)
    if (mod (k, 3) == 0)
        printf ('<tr>', >> "index.html")
    ;
    if (wcscal == "WCS_FAILED")
	printf ('<td><a href="%s">%s</a><br>%s<br>%s<br>\
	    <font color="red">%s</font><br>%s, %s<br>%.20s\
	    <td><a href="%s.jpg"><img src="%s.jpg"></a>\n',
	    s2//"_hdr.txt", dtnsanam, dateobs, propid,
	    wcscal, exptime, magzero, title, s2//"_oj2", s2//"_oj1",
	    >> "index.html")
    else
	printf ('<td><a href="%s">%s</a><br>%s<br>%s\
	    <br>%s, %s<br>%.20s\
	    <td><a href="%s.jpg"><img src="%s.jpg"></a>\n',
	    s2//"_hdr.txt", dtnsanam, dateobs, propid,
	    exptime, magzero, title, s2//"_oj2", s2//"_oj1",
	    >> "index.html")
}
list = ""


# Finish up.
printf ('</table></body></html>\n', >> "index.html")

plexit COMPLETED
