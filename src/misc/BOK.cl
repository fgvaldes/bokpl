#{ BOK.CL -- Pipeline configuration script.
# This may do any valid IRAF setup but it is mostly intended for defining
# global variables.  Note that these become CL package parameters.

string	calprops = ""		{prompt="PropIDs for cals"}
string	objprops = ""		{prompt="PropIDs for objects"}
string  ccds = ""		{prompt="CCDs to process"}
int	dcp_nextend = 62	{prompt="Required number of extensions"}
string	skip = ""		{prompt="Pipelines/modules to skip"}
string	halt = ""		{prompt="Pipelines/modules to halt"}
int     top_filtrig = 0         {prompt="Num of filters to trigger in parallel"}
string	overscan = "spline3"	{prompt="Overscan method (minmax|legendre)"}
int	ooverscan = 20		{prompt="Overscan order"}
bool	zerocor = yes		{prompt="Apply zero calibration?"}
bool	zro_review = no		{prompt="Review master zeros?"}
bool	do_xtalk = yes		{prompt="Do crosstalk correction?"}
bool	xt_wrap = yes		{prompt="Apply wraparound correction?"}
real	xt_wrapradius = 0.	{prompt="Grow radius around wraparound (pixels)"}
bool	do_lincor = no		{prompt="Do linearity correction?"}
bool	do_starflat = yes	{prompt="Do starflat correction?"}
bool	do_ampflat = yes	{prompt="Do ampflat correction?"}
real	do_cr = 3.5		{prompt="Cosmic ray FWHM threshold? (pixels)"}
bool	do_streak = yes		{prompt="Do streak detection?"}
bool	do_sky = yes		{prompt="Do sky removal?"}
bool    fil_cklimits = yes      {prompt="Check limits in filevl?"}
bool	make_lincor = no	{prompt="Make linearity correction?"}
bool	make_dillcor = no	{prompt="Make dome illumination correction?"}
bool	wcsglobal = no		{prompt="Do only global WCS?"}
real	osi_satcor = 0.95	{prompt="Saturation correction"}
real    osi_xshift = 0.         {prompt=" Right amp shift (pix)"}
real    osi_yshift = 0.         {prompt=" Upper amp shift (pix)"}
string	osi_interp = "1,3,4"    {prompt="Flags to iterpolate"}
bool	fmi_review = no		{prompt="Review dome flats?"}
int     sky_smooth = 0          {prompt="Smooth ACE sky?"}
int     sky_blksize = -20       {prompt="ACE sky block size"}
int     sky_nsubblks = 2        {prompt="ACE no. of sky subblocks"}
bool	sky_review = yes	{prompt="Review master dark sky evaluations?"}
int	cal_nmin = 5		{prompt="Min number of exposures in cal stack"}
int	cal_nmax = 30		{prompt="Max number of exposures in cal stack"}
bool	red_bleed = yes		{prompt="Do CP bleed mask step?"}
string	photref = "usno"	{prompt="Photometric reference"}
string	grp_illum = "yes"	{prompt="Illumination (yes|no|red|redcal|cal)"}
real	dra = 0			{prompt="RA shift"}
real	ddec = 0		{prompt="DEC shift"}
int	grp_nillum = 5		{prompt="Minimum exposures for illumination"}
bool	sky_pupil = yes		{prompt="Do pupil subtraction?"}
bool	sky_sub = yes		{prompt="Do sky subtraction?"}
real	sft_minexptime = 45	{prompt="Min exposure for sky stacks"}
int	sft_nmin = 20           {prompt="Minimum for sky flat"}
bool	sft_smooth = yes	{prompt="Smooth sky flat?"}
string  sft_func = "spline3"    {prompt="Smoothing function"}
string	sfc_mode = "subtract"	{prompt="Sky flat correction mode"}
string	ill_run = ""		{prompt="Illumination run"}
bool	ill_mkcor = yes		{prompt="Make illumination correction?"}
int	ill_minexp = 40		{prompt="Minimum exptime for illumination"}
string	pgt_type = "fit"	{prompt="Pupil template type (data|fit)"}
string	pgr_scale = "ccd"	{prompt="Pupil template scaling (ccd|global)"}
real	sky_gainadj = 0.	{prompt="Apply gain adjustment up to this amount"}
real    rsp_sep = 600           {prompt="Grouping (arcsec)"}
real	rsp_pixsize = 0.27	{prompt="Resample pixel size (arcsec/pix)"}
bool	rsp_global = yes	{prompt="Resample with global sky subtraction?"}
bool	rsp_local = yes		{prompt="Resample with local sky subtraction?"}
real	ssk_diffsig = 3.5	{prompt="Difference detection sigma"}
string	ssk_diffconv = ""	{prompt="Difference detection convolution"}
int	stk_mov = 0		{prompt="Find moving objects?"}
bool	stk_autompc = no	{prompt="Automatically send to MPC?"}
string	stk_alert = "!Alert_operator"	{prompt="Alert command"}
string	stk_exclude = "31"	{prompt="CCDs to exclude in stacks"}
bool	stk_addast = no		{prompt="Add asteroids?"}
string	nsatrim = ""		{prompt="CCD trim for InstCal"}
string	rmptrim = ""		{prompt="CCD trim for Resampled"}
bool	dts_compress = yes	{prompt="Compress data products?"}
bool	dts_submit = no		{prompt="Auto submit from dts pipeline?"}

keep
