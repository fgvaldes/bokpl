#!/bin/env pipecl
#
# OMIWCS -- Update the WCS.
# There is a nttools bug where use of fits catalogs for astcat fail.


if (strstr("MASK",dataset.name) > 0)
    plexit SKIP
;

if (strstr ("wcs", skip) > 0)
    plexit COMPLETED
;

#string	onerr = "COMPLETED"
string	onerr = "HALT"
string	exitcode = ""
string	catextn = "cat.fits"
#string	catextn = "cat.txt"
#string	astextn = "astcat.fits"
string	astextn = "astcat.txt"

string	proj   = "tpv"
string	proj1  = "tnx"
string	outwcs = "tpv"
#string	outwcs = "zpx"
int	xxorder = 5
int	xyorder = 5
int	yxorder = 5
int	yyorder = 5
string	xterms = "half"
real	search = 200.
#real	search = 300.
#real	rsearch = 1.
real	rsearch = 3.
real	match = 4
real	fracmatch = 0.40
#real	fracmatch = 0.20
#real	fracmatch = 0.05
real	nmin = 50.
#real	nmin = 25.
#real	maxwcsrms = 0.1
real	maxwcsrms = 0.5
#int	ngrid = 900
int	ngrid = 100
real	rej = 2.5
int	maxit = 5
bool	debug = NO

real	astmaprms = INDEF
real	ccmaprms = 0

# To run this interactively set "omiwcs" in the halt pipeline variable.
# When it halts you can run this with "runmodule omiwcs".
# If it completes successfully you can set the flag to 'c'.
if (strstr("omiwcs",halt) > 0 && defvar("RUNMODULE") == NO)
    plexit HALT
else if (strstr("omiwcs",halt) > 0 && defvar("RUNMODULE") == YES)
    debug = YES
;

int	ccd
string	caldate, filt, mjd
string	ra, dec, wcsrms, rashift, decshift, reffilt
real	photclam, photbw
file	dir, base, refcat, im, bpm, obm, cat, astcat, wcsdb, wcs, sky, sig

# Tasks and packages.
nfextern
ace

delete ("*"//astextn)

if (nhppsargs.application == "BOK") {
    match = 7
    maxwcsrms = 1.
    if (make_dillcor == YES)
	onerr = "COMPLETED"
    else
	onerr = "COMPLETED"
}
;

#if (strstr("WCS",dataset.name) > 0) {
#xxorder -= 1
#xyorder -= 1
#yxorder -= 1
#yyorder -= 1
#}
#;

# Set images and catalogs.
match ("-[0-9].fits", "*.osi", print-, > "omiwcs_ims.tmp")
list = "omiwcs_ims.tmp"
while (fscan (list, im) != EOF) {
    i = strldx ("/", im)
    j = strldx (".", im)
    dir = substr (im, 1, i)
    base = substr (im, i+1, j-1)
    ccd = int (substr (im, j-1, j-1))
    if (access(dir//base//"_"//catextn)) {
#s1 = "omiwcs_"//base//".tmp"
#copy (dir//base//"_"//catextn, s1)
#print (s1, >> "omiwcs_cats.tmp")
#tchcol (s1, "MAG", "MAGAPP", "", "")
#tchcol (s1, "MAGISO", "MAG", "", "")
	print (dir//base//"_"//catextn, >> "omiwcs_cats.tmp")
	print (base//"_"//astextn, >> "omiwcs_astcats.tmp")
    } else {
	printf ("WARNING: Catalog not found (%s)\n", base//"_"//catextn)
        exitcode = onerr
    }
}
list = ""

# Set WCS info and astrometric reference catalogs.
# Merge them because astmatch expects a single catalog.
if (exitcode == "") {
    hselect (im, "$MJD-OBS,$FILTER,$DTCALDAT", yes) | scan (mjd, filt, caldate)

    photclam = INDEF; photbw = INDEF; x = 6000
    getcal ("filtwave", filter=filt)
    if (getcal.nfound > 0) {
	i = fscan (getcal.value, photclam, photbw)
	if (i > 0)
	    x = photclam
	;
    }
    ;

    # Try Gaia and then PS1.
    if (strstr("D51", dataset.name) > 0)
	print ("gaia", >> "omiwcs_refname.tmp")
    else
	print ("gaia\nps1", >> "omiwcs_refname.tmp")
    list = "omiwcs_refname.tmp"; k = 0
    while (fscan (list, s1) != EOF) {
	if (s1 == "ps1") {
	    if (x < 5100)
		reffilt = "g"
	    else if (x < 5850)
		reffilt = "V"
	    else if (x < 7000)
		reffilt = "r"
	    else if (x < 8300)
		reffilt = "i"
	    else
		reffilt = "z"
	} else
	    reffilt = "G"
	getrefcats ("omiwcs_cats.tmp", s1, reffilt, buffer=0.2,
	    racol="WX", deccol="WY", raunits="hr", verbose-,
	    > "omiwcs_refcats.tmp")
	count ("omiwcs_refcats.tmp") | scan (i)
	if (i == 0)
	    next
	;
	if (s1 == "ps1")
	    s1 = "PS1"
	else if (s1 == "gaia")
	    s1 = "GAIA"
	;
	refcat = s1
	printf ("refcat = %s, reffilt=%s\n", refcat, reffilt)
	if (s1 == "PS1") {
	    tmerge ("@omiwcs_refcats.tmp", "omiwcs_refcat1_tmp.fits",
		option="append")
	    tselect ("omiwcs_refcat1_tmp.fits", "omiwcs_refcat_tmp.fits",
		"mag>=15&&mag<=22")
	    delete ("omiwcs_refcat1_tmp.fits")
	} else
	    tmerge ("@omiwcs_refcats.tmp", "omiwcs_refcat_tmp.fits",
		option="append")
	break
    }
    list = ""
    if (i == 0) {
	printf ("%s: No reference catalogs found\n", onerr)
	exitcode = onerr
    }
    ;
} else
    print dummy > dev$null
;

# Match source and reference catalogs.
if (exitcode == "") {
    # Match the catalogs against the reference catalog.
    touch ("omiwcs_astmatch.tmp")
    astmatch.xi = 0.; astmatch.eta=0.; astmatch.theta=0.
    iferr {
	astmatch ("@omiwcs_cats.tmp", "omiwcs_refcat_tmp.fits",
	    matchcat="@omiwcs_astcats.tmp",
	    imcatdef="@"//nhppsargs.srcdir//"catmatch.def",
	    refcatdef="@"//nhppsargs.srcdir//"refcat.def",
	    imwcs="", imfilter="", reffilter="", mosaic=yes, search=search,
	    rsearch=rsearch, rstep=0.2, histimages="", nim=1000, nref=300,
	    nmin=nmin, fwhm=2., match=match, fracmatch=fracmatch,
	    erraction="abort", logfile="omiwcs_astmatch.tmp")
    } then {
	exitcode = onerr
    }
    ;

#    if (exitcode == "")
#	delete ("omiwcs_refcat_tmp.fits")
#    ;
    rename ("omiwcs_refcat_tmp.fits", "omiwcs_refcat"//reffilt//"_tmp.fits")
    concat ("omiwcs_astmatch.tmp")
    concat ("omiwcs_astcats.tmp", >> dataset.fdelete)
}
;

if (exitcode == "") {
    # Compute a global correction and add a constraining grid.
    touch ("omiwcs_astmap.tmp")
    iferr {
	astmap ("@omiwcs_astcats.tmp", wcs="", catdef="", filter="",
	    mosaic=yes, fitgeometry="general", reject=3.,
	    interactive=debug, graphics="stdgraph", cursor="", rms=maxwcsrms,
	    ngrid=ngrid, erraction="abort", logfile="omiwcs_astmap.tmp")
    } then {
	exitcode = onerr
    }
    ;

    concat ("omiwcs_astmap.tmp")

    # Check RMS.
    match ("rms = ", "omiwcs_astmap.tmp") | scan (s1, s1, line)
    x=0.; y=0.; i = fscanf (line, "(%g, %g)", x, y)
    astmaprms = sqrt (x*x + y*y)
    if (max(x,y) > maxwcsrms) {
	printf ("ERROR: %s\n", "rms = "//line)
	exitcode = onerr
    } else {
	match ("new tangent point", "omiwcs_astmap.tmp") |
	    translit ("STDIN", "(,)", del+) | scan (s1, s1, s1, s1, ra, dec)
	printf ("%5.3f\n", astmaprms) | scan (wcsrms)
	match ("shift = ", "omiwcs_astmap.tmp") | scan (s1, s1, s1, s1, line)
	x=0.; y=0.; i = fscanf (line, "(%g, %g)", x, y)
	printf ("%.2f %.2f\n", x, y) | scan (rashift, decshift)
    }
}
;

# Set projection.
getbokwcs (im)
match ("projection", getbokwcs.wcsdb) | scan (s1, proj)
if (proj == "zpx") {
    proj1 = "omiwcs_proj.tmp"
    print (proj, > proj1)
    match ("projp", getbokwcs.wcsdb) | sort | uniq (>> proj1)
}
;

# Do WCS solution.
if (exitcode == "") {
    touch ("omiwcs_ccmap.tmp")
    list = "omiwcs_ims.tmp"
    while (fscan(list, im) != EOF) {
	i = strldx ("/", im)
	j = strldx (".", im)
	dir = substr (im, 1, i)
	base = substr (im, i+1, j-1)
	ccd = int (substr (im, j-1, j-1))
	astcat = base // "_"//astextn
	#wcsdb = base // "_wcsdb.txt"
	wcsdb = substr(im,i+1,j-3) // "_wcsdb.txt"
	if (access(wcsdb)==NO) {
	    hselect (im, "DTNSANAM", yes) | scan (s1)
	    s1 = substr (s1, 1, strldx("_",s1)-1)
	    printf ("# WCSASTRM = '%s %s'\n", s1, getbokwcs.focusval, > wcsdb)
	    printf ("# CALDATE = '%s'\n", caldate, >> wcsdb)
	    printf ("# FILTER = '%s'\n", filt, >> wcsdb)
	    printf ("# MJD-OBS = %s\n", mjd, >> wcsdb)
	    printf ("# FOCUSVAL = '%s'\n", getbokwcs.focusval, >> wcsdb)
	    printf ("\n", >> wcsdb)
	}
	;
	if (wcsglobal) {
	    tselect (astcat, "omiwcs_grid.tmp", "mref<-98")
	    maxit = 0
	    s1 = "omiwcs_grid.tmp"
	} else {
	    #s1 = astcat
	    tselect (astcat, "omiwcs_grid.tmp", "mref>-98")
	    s1 = "omiwcs_grid.tmp"
	}

	iferr {
	    ccmap (s1, wcsdb, solutions=ccd, images="",
		results="", xcolumn=6, ycolumn=7, lngcolumn=1, latcolumn=2,
		xmin=1., xmax=2048., ymin=1., ymax=4096., lngunits="",
		latunits="", insystem="j2000", refpoint="user",
		xref="INDEF", yref="INDEF", lngref=ra, latref=dec,
		refsystem="INDEF", lngrefunits="", latrefunits="",
		projection=proj1, fitgeometry="general",
		function="polynomial", xxorder=xxo, xyorder=xyo,
		xxterms=xterms, yxorder=yxo, yyorder=yyo, yxterms=xterms,
		maxiter=maxit, reject=rej, update=no, pixsystem="logical",
		verbose=yes, interactive=debug, graphics="stdgraph",
		cursor="", > "omiwcs_ccmap1.tmp")
	    concat ("omiwcs_ccmap1.tmp", "omiwcs_ccmap.tmp", append+)
	    print (wcsdb, >> dataset.fdelete)
	} then {
	    concat ("omiwcs_ccmap1.tmp", "omiwcs_ccmap.tmp", append+)
	    exitcode = onerr
	    break
	}
	;

	# Set RMS.
	match ("rms: ", "omiwcs_ccmap1.tmp") | scan (s1, s1, s1, s1, s1, line)
	x=0.; y=0.; i = fscan (line, x, y)
	z = sqrt (x*x + y*y)
	ccmaprms = max (ccmaprms, z)
	printf ("WCSRMS1 = %.3g\n", z)
	nhedit (im, "WCSRMS1", z, "[arcsec] WCS solution RMS", add+)
    }
    list = ""

    concatenate ("omiwcs_ccmap.tmp")
    line = ""; match ("INDEF", "omiwcs_ccmap.tmp") | scan (line)
    if (line != "") {
	printf ("%s: ccmap failed to produce a solution\n", onerr)
        exitcode = onerr
    }
    ;

    printf ("%5.3f\n", ccmaprms) | scan (wcsrms)
}
;

# Use the astmap global solution from the default WCSDB.
if (exitcode == "" && (ccmaprms == 0 || 1.5 * astmaprms < ccmaprms)) {
    printf ("Use ASTMAP solution: ccmaprms = %g, astmaprms = %g\n", ccmaprms, astmaprms)
    delete ("omiwcs_ccmap.tmp")
    touch ("omiwcs_ccmap.tmp")
    list = "omiwcs_ims.tmp"; ccmaprms = 0
    while (fscan(list, im) != EOF) {
	i = strldx ("/", im)
	j = strldx (".", im)
	dir = substr (im, 1, i)
	base = substr (im, i+1, j-1)
	ccd = int (substr (im, j-1, j-1))
	astcat = base // "_"//astextn
	#wcsdb = base // "_wcsdb.txt"
	wcsdb = substr(im,i+1,j-3) // "_wcsdb.txt"
	tselect (astcat, "omiwcs_grid.tmp", "mref<-98")
	maxit = 0
	s1 = "omiwcs_grid.tmp"

	iferr {
	    ccmap (s1, wcsdb, solutions=ccd, images="",
		results="", xcolumn=6, ycolumn=7, lngcolumn=1, latcolumn=2,
		xmin=1., xmax=2048., ymin=1., ymax=4096., lngunits="",
		latunits="", insystem="j2000", refpoint="user",
		xref="INDEF", yref="INDEF", lngref=ra, latref=dec,
		refsystem="INDEF", lngrefunits="", latrefunits="",
		projection=proj1, fitgeometry="general",
		function="polynomial", xxorder=xxo, xyorder=xyo,
		xxterms=xterms, yxorder=yxo, yyorder=yyo, yxterms=xterms,
		maxiter=maxit, reject=rej, update=no, pixsystem="logical",
		verbose=yes, interactive=debug, graphics="stdgraph",
		cursor="", > "omiwcs_ccmap1.tmp")
	    concat ("omiwcs_ccmap1.tmp", "omiwcs_ccmap.tmp", append+)
	    print (wcsdb, >> dataset.fdelete)
	} then {
	    concat ("omiwcs_ccmap1.tmp", "omiwcs_ccmap.tmp", append+)
	    exitcode = onerr
	    break
	}
	;

	# Set RMS.
	match ("rms: ", "omiwcs_ccmap1.tmp") | scan (s1, s1, s1, s1, s1, line)
	x=0.; y=0.; i = fscan (line, x, y)
	z = sqrt (x*x + y*y)
	ccmaprms = max (ccmaprms, z)
	nhedit (im, "WCSRMS1", z, "[arcsec] WCS solution RMS", add+)
    }
    list = ""

    concatenate ("omiwcs_ccmap.tmp")
    line = ""; match ("INDEF", "omiwcs_ccmap.tmp") | scan (line)
    if (line != "") {
	printf ("%s: ccmap failed to produce a solution\n", onerr)
        exitcode = onerr
    }
    ;

    if (ccmaprms < astmaprms)
	printf ("%5.3f\n", ccmaprms) | scan (wcsrms)
    ;
} else
    printf ("%5.3f\n", ccmaprms) | scan (wcsrms)
;

# Update WCSDB.
if (exitcode == "")
    printf ("\n# WCSRMS = %g\n", wcsrms, >> wcsdb)
;

# Update headers.
list = "omiwcs_ims.tmp"; delete ("omiwcs_cats.tmp")
while (fscan(list, im) != EOF ) {
    i = strldx ("/", im)
    j = strldx (".", im)
    dir = substr (im, 1, i)
    base = substr (im, i+1, j-1)
    ccd = int (substr (im, j-1, j-1))
    bpm = dir // base // "_bpm"
    obm = dir // base // "_obm"
    sky = dir // base // "_sky"
    sig = dir // base // "_sig"
    cat = base // "_"//catextn
    astcat = base // "_"//astextn
    #wcsdb = base // "_wcsdb.txt"
    wcsdb = substr(im,i+1,j-3) // "_wcsdb.txt"

    if (access (cat))
        delete (cat)
    ;

    if (exitcode != "") {
	if (access(dir//cat))
	    copy (dir//cat, cat)
	;
	if (outwcs != proj) {
	    chwcs (im, out="", wcs=outwcs, xxo=5, xyo=5, yxo=5, yyo=5)
	    chwcs (bpm, out="", wcs=outwcs, xxo=5, xyo=5, yxo=5, yyo=5,
	        verb-)
	}
	;
	nhedit (im, "WCSCAL", "Failed", "WCS calibration", add+)
	if (imaccess(bpm))
	    nhedit (bpm, "WCSCAL", "Failed", "WCS calibration", add+)
	;
	if (imaccess(obm))
	    nhedit (obm, "WCSCAL", "Failed", "WCS calibration", add+)
	;
	if (imaccess(sky))
	    nhedit (sky, "WCSCAL", "Failed", "WCS calibration", add+)
	;
	if (imaccess(sig))
	    nhedit (sig, "WCSCAL", "Failed", "WCS calibration", add+)
	;
	if (access(cat))
	    thedit (cat, "WCSCAL", "Failed")
	;
	next
    }
    ;

    ccsetwcs (im, wcsdb, ccd, update+, verbose-,
	>> "omiwcs_ccmap.tmp")
    ccsetwcs (bpm, wcsdb, ccd, update+, verbose-,
	>> "omiwcs_ccmap.tmp")
    ccsetwcs (obm, wcsdb, ccd, update+, verbose-,
	>> "omiwcs_ccmap.tmp")
    acesetwcs (dir//cat, cat, wcsdb, ccd,
	catdef="", verbose-, >> "omiwcs_ccmap.tmp")
    print (cat, >> dataset.fdelete)
    print (cat, >> ".omidone_delete.tmp")
    print (dir//cat, >> ".omidone_delete.tmp")

    if (outwcs != proj) {
	chwcs (im, out="", wcs=outwcs, xxo=5, xyo=5, yxo=5, yyo=5)
	chwcs (bpm, out="", wcs=outwcs, xxo=5, xyo=5, yxo=5, yyo=5, verb-)
    }
    ;

    nhedit (im,  "WCSCAL", "Successful", "WCS calibration", add+)
    nhedit (bpm, "WCSCAL", "Successful", "WCS calibration", add+)
    nhedit (obm, "WCSCAL", "Successful", "WCS calibration", add+)
    nhedit (sky, "WCSCAL", "Successful", "WCS calibration", add+)
    nhedit (sig, "WCSCAL", "Successful", "WCS calibration", add+)
    thedit (cat, "WCSCAL", "Successful")
    nhedit (im,  "ASTRMCAT", refcat, "Astrometric ref. catalog", add+)
    nhedit (bpm, "ASTRMCAT", refcat, "Astrometric ref. catalog", add+)
    nhedit (obm, "ASTRMCAT", refcat, "Astrometric ref. catalog", add+)
    nhedit (sky, "ASTRMCAT", refcat, "Astrometric ref. catalog", add+)
    nhedit (sig, "ASTRMCAT", refcat, "Astrometric ref. catalog", add+)
    thedit (cat, "ASTRMCAT", refcat)
    hedit (im, "RA", ra)
    thedit (cat, "RA", ra)
    hedit (im, "DEC", dec)
    thedit (cat, "RA", ra)
    nhedit (im, "WCSDRA", rashift, "[arcsec] WCS tangent point shift", add+)
    nhedit (im, "WCSDDEC", decshift, "[arcsec] WCS tangent point shift", add+)
    nhedit (im, "WCSRMS", wcsrms, "[arcsec] WCS solution RMS", add+)
}
list = ""

if (exitcode != "" && exitcode != "COMPLETED") {
    touch (dataset.fprotect)
    mkgraphic ("@omiwcs_ims.tmp", "omiwcs", "physical", "gif", 8)
    rename ("omiwcs.gif", "omiwcs_gif.tmp")
    plexit (exitcode)
}
;

plexit COMPLETED
