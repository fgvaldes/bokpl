# OMIMKDB -- Take the final OMI output and produce FOCUSVAL WCSDB files.
#
# This requires the filename conventions preserved in the final run results.

procedure omimkdb (dblist)

file	dblist			{prompt="List of OMI wcsdb files"}

struct	*fd

begin
	int	i, j, ccdnum
	real	f1, f2, f3
	string wcsdb, ccddb, ccd, exp, nsa, junk

	fd = dblist
	while (fscan (fd, ccddb) != EOF) {
	    i = strldx ("-", ccddb)
	    j = strldx ("_", ccddb)
	    ccdnum = int (substr(ccddb,i+1,j-1))
	    ccd = substr (ccddb, 1, j-1)
	    exp = substr (ccddb, 1, i-1)
	    wcsdb = exp // "_wcs.db"

	    # Get the focus value.
	    match ("DTNSANAM", exp//"_hdr.txt") | scan (junk, nsa)
	    nsa = substr (nsa, 1, strldx("_",nsa)-1)
	    match ("FOCUSVAL", exp//"_hdr.txt") |
	        translit ("STDIN", "'*", " ") | scan (junk, f1, f2, f3)

	    if (!access(wcsdb))
		printf ("# WCSASTRM = '%s %6.4f %6.4f %6.4f'\n",
		    nsa, f1, f2, f3, > wcsdb)

	    printf ("\n", >> wcsdb)
	    sed ("'s+wcs$+"//ccdnum//"+;s+logical+physical+'",
	        ccddb, >> wcsdb)
	}
	fd = ""
end
