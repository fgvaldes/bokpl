#!/bin/env pipecl
#
# OMIXTALK - Apply crosstalk correction and separate into single images.

bool	dowrap = yes
int	wrapthresh = 0
real	bpmthresh = -500	# Mosaic1.1
real	wrapradius = 3.

string	status = "COMPLETED"
file	xtalkfile, mef, sif

# Packages and tasks.
nfextern
msctools
xtalk

# Pipeline parameters
#dowrap = xt_wrap
dowrap = NO
wrapradius = xt_wrapradius

# Filename.
concat ("omisetup.tmp") | scan (mef)
i = strstr (".fits", mef)
if (i > 0)
    mef = substr (mef, 1, i-1)
;

# Get ccdnames file.
getcal ("ccdnames", path+, image=mef//"[0]")
if (getcal.nfound < 1) {
    getcal ("ccdnames", path+, image=mef//"[0]", sql="STDOUT")
    plexit FATAL
}
;
copy (getcal.value, "omixtalk_ccdnames.tmp")

if (do_xtalk) {
    # Get crosstalk coefficient file.
    getcal ("xtalkfile", path+, image=mef//"[0]", mjd="!mjd-obs",
        sql="STDOUT", results="STDOUT")
    if (getcal.nfound < 1) {
	getcal ("xtalkfile", path+, image=mef//"[0]", mjd="!mjd-obs",
	    sql="STDOUT")
	plexit FATAL
    }
    ;
    xtalkfile = getcal.value
    xtalkfile = substr (getcal.value, strldx("/",getcal.value)+1, 999)
    copy (getcal.value, "omixtalk_xtalkfile.tmp")
} else {
    xtalkfile = "NONE"
    touch ("omixtalk_xtalkfile.tmp")
}
;

# Apply wraparound correction before crosstalk.
if (dowrap) {
    mscextensions (mef, output="file", > "omixtalk_ext.tmp")
    list = "omixtalk_ext.tmp"
    while (fscan (list, sif) != EOF) {
	getcal ("saturate", image=sif, imageid="!imageid", mjd="!mjd-obs")
	if (getcal.nfound == 0) {
	    getcal ("saturate", image=sif, imageid="!imageid", sql="STDOUT")
	    break
	}
	;
	
	if (int(getcal.value) < wrapthresh) {
	    printf ("No wraparound correction:    %s\n", sif)
	} else {
	    printf ("Apply wraparound correction: %s\n", sif)
	    s1 = "[]"; hselect (sif, "DATASEC", yes) | scan (s1)
	    if (wrapradius > 0) {
		imexpr ("a<0.9*median(a)?1: 0", "omixtalk_bpmtmp.pl",
		    sif//s1, outtype="int")
		imcopy ("omixtalk_bpmtmp.pl", "omixtalk_bpm1tmp.pl", v-)
		imreplace ("omixtalk_bpm1tmp.pl", 2, lower=1, radius=wrapradius)
		imexpr ("b>0?a+65535:(c>0?65535:a)", "omixtalk_tmp", sif//s1,
		    "omixtalk_bpmtmp.pl", "omixtalk_bpm1tmp.pl", outtype="int")
	    } else {
		imexpr ("a<0.9*median(a)?a+65535:a", "omixtalk_tmp",
		    sif//s1, outtype="int")
	    }
	    ;
	    imcopy ("omixtalk_tmp", sif//s1, v-)
	    imdelete ("omixtalk_*tmp.*"); flpr
	}
	;
    }
    list = ""
    delete ("omixtalk_ext.tmp")
}
;
if (dowrap && getcal.nfound == 0)
    plexit FATAL
;

# Do crosstalk and splitting.
xtalkcor (mef, mef, "", "omixtalk_xtalkfile.tmp", logfile=dataset.lfile,
    verbose+, section="!datasec", bpmthreshold=bpmthresh, split+,
    fextn="fits", flip-, noproc-)
files (mef//"_*", > "omixtalk_sif.tmp")
concat ("omixtalk_sif.tmp", dataset.fdelete, append+)
concat ("omixtalk_sif.tmp", ".omidone_delete.tmp", append+)

# Set OSI dataset names.
list = "omixtalk_sif.tmp"
while (fscan (list, sif) != EOF) {
    hselect (sif, "AMPID", yes) | scan (line)
    match (line, "omixtalk_ccdnames.tmp", meta-) | scan (j, k)
    hedit (sif, "XTALKFIL", xtalkfile, add+)
    hedit (sif, "AMPNUM", j, add+)
    hedit (sif, "CCDNUM", k, add+)

    # Break up for basic calibration.
    # This can be by AMP or CCD.
    printf ("omixtalk-%d.tmp\n", k) | scan (s2)
    if (access(s2) == NO)
	fspath (s2, >> "omixtalk.tmp")
    fspath (sif, >> s2)
}
list = ""; delete ("omixtalk_sif.tmp")
delete ("*.osi")
	
plexit COMPLETED
