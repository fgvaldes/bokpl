#!/bin/env pipecl
#
# SKYCOR -- Apply final sky filtering and subtraction.

int	xwin = 30
int	ywin = 64
int	yshift = 32
int	ywinobm = 8
int	splineord = 1

int	type
real	gainnorm
file	in, out, bpm, obm, sky, im, ampinfo, def, stat
string	ccd, amp, sec, pars
struct	*fd

# Task and packages.
nfextern
ace
def = nhppsargs.srcdir//"skycor.def"

# Set input: list of CCDs in a single exposure divided between image and mask.
match ("[0-9].fits", dataset.ifile) | sort | unique (> "skycor_ims.tmp")
match ("[0-9]_obm.pl", dataset.ifile) | sort | uniq (> "skycor_obm.tmp")
count ("skycor_ims.tmp") | scan (i)
count ("skycor_obm.tmp") | scan (j)
if (i == 0 || i != j) {
    printf ("WARNING: Images and object masks don't match\n")
    plexit COMPLETED
}
;

# Loop though CCDs.
list = "skycor_ims.tmp"
while (fscan (list, in) != EOF) {

    # Set filenames: input bad pixel and object masks, output image.
    im = substr (in, strldx("/",in)+1, strldx(".",in)-1)
    match (im, "skycor_obm.tmp") | scan (obm)
    hselect (in, "GAINNORM", yes) | scan (gainnorm)
    hselect (in, "BPM", yes) | scan (bpm)
    out = im // ".fits"
    sky = im // "_sky.fits"

    # Set amp information: image section and filtering algorithm type.
    hselect (in, "CCDNAME", yes) | scan (ccd)
    getcal ("ampinfo", path+, image=in, imageid=ccd, mjd="!mjd-obs")
    if (getcal.nfound == 0) {
	getcal ("ampinfo", path+, image=in, imageid=ccd, mjd="!mjd-obs",
	    sql="STDOUT")
	plexit FATAL
    }
    ;
    ampinfo = getcal.value

    # Extend the mask along the filtering direction.
    imexpr ("a>0?1000: 0", "skycor1_obmtmp.pl", obm)
    obm = im // "_obm.pl"
    print (obm, >> dataset.fdelete)
    boxcar ("skycor1_obmtmp.pl", obm, 1, ywinobm, boundary="reflect")
    imreplace (obm, 1, low=1)
    imdelete ("skycor1_obmtmp.pl")

    # Do each amplifier in the CCD.
    fd = ampinfo
    while (fscan (fd, amp, sec, s1, s1, s1, type, stat) != EOF) {
	printf ("  Fix %s:%s\n", ccd, amp)

	# Temp names.
        im = "skycor" // amp // "_tmp.fits"
	print (im, >> "skycor_amps.tmp")

	# Extract amplifier section. 
	imcopy (in//sec, "skycor1_tmp.fits", v-)
	imcopy (obm//sec, "skycor1obm_tmp.pl", v-)

	# First pass to minimize effect of sources by interpolating across them.
	fixpix ("skycor1_tmp", "skycor1obm_tmp", linterp=2)

	# Extend the mask along lines by shifting left and right.
	imshift ("skycor1_tmp,skycor1obm_tmp", "skycorR_tmp,skycorRobm_tmp.pl",
	    xshift=0, yshift=yshift, boundary="reflect")
	imshift ("skycor1_tmp,skycor1obm_tmp", "skycorL_tmp,skycorLobm_tmp.pl",
	    xshift=0, yshift=yshift, boundary="reflect")

	# Replace data in shifted regions by nearby data.
	imexpr ("b==0?a:(d==0?c:(f==0?e:a))", "skycor2_tmp",
	    "skycor1_tmp", "skycor1obm_tmp",
	    "skycorR_tmp", "skycorRobm_tmp",
	    "skycorL_tmp", "skycorLobm_tmp")

	# To further restrict non-sky set 5 sigma clipping levels.
	imstat ("skycor2_tmp", fields="mean,stddev", format-) | scan (y, z)
	x = y - 5 * z; y = y + 5 * z

	# Median smooth to remove noise and unmasked faint sources and
	# enhance the pattern bias.
	#   Type 1 is for line-by-line pattern noise.
	#   Type 2 is for a broader pattern noise.

	if (type == 1 || type == 3)
	    fmedian ("skycor2_tmp", "skycormed_tmp", 1, ywin,
		boundary="reflect", zmin=x, zmax=y, v-)
	else
	    fmedian ("skycor2_tmp", "skycormed_tmp", ywin, xwin,
		boundary="reflect", zmin=x, zmax=y, v-)

	# Detect any remaining lumps in object mask and segment smooth sky.
	iferr {
	    s2 = ""
	    aceall ("skycormed_tmp", masks="", logfiles="", verbose=2,
		catalogs="", catdefs=def, catfilter="",
		order="", nmaxrec=INDEF, magzero="INDEF",
		fwhm=4., cafwhm=2., gwtsig=INDEF, gwtnsig=INDEF,
		objmasks="skycorskyobm_tmp", omtype="boolean",
		skyotype="sky", skyimages="skycorsky_tmp", sigimages="",
		skies="", sigmas="", rskies="", rsigmas="", fitstep=100,
		fitblk1d=10, fithclip=2., fitlclip=3., fitxorder=1,
		fityorder=1, fitxterms="half", blkstep=1, blksize=-20,
		blknsubblks=2, hdetect=yes, ldetect=no, updatesky=yes,
		bpdetect="1-100", bpflag="1-100",
		convolve="bilinear 3 3", hsigma=8., lsigma=10.,
		neighbors="8", minpix=100, sigavg=4., sigmax=4.,
		bpval=INDEF, rfrac=0.5, splitstep=0.4, splitmax=INDEF,
		splitthresh=5., sminpix=8, ssigavg=10., ssigmax=5.,
		ngrow=5, agrow=3.)
	} then {
	    s2 = "ERR"
	    imcopy (in//sec, im, v-)
	}
	;

	# Subtract pattern bias.
	if (s2 != "ERR") {
	    # Remove non-pattern sky so it is not modified.
	    imexpr ("a-b", "skycorsky1_tmp", "skycormed_tmp", "skycorsky_tmp")

	    # Interpolate across the lumps found by ACE.
	    copy ("skycorsky1_tmp.fits", "skycorbkg1_tmp.fits", v-)
	    fixpix ("skycorbkg1_tmp", "skycorskyobm_tmp[1]", linter=2)
	    copy ("skycorsky1_tmp.fits", "skycorbkg2a_tmp.fits", v-)
	    fixpix ("skycorbkg2a_tmp", "skycorskyobm_tmp[1]", linter=1)
	    imexpr ("min(2*b,max(-2*b,a))",
	        "skycorbkg2_tmp", "skycorbkg2a_tmp", gainnorm)

	    # Extract the pattern noise.
	    if (type == 1 || type == 3)
		fit1d ("skycorbkg2_tmp", "skycorbkg3_tmp", "fit", bpm="",
		    axis=2, interactive=no, sample="*", naverage=1,
		    function="spline3", order=splineord, low_reject=6.,
		    high_reject=2.5, niterate=3, grow=5.)
	    else if (type == 2) {
		hselect ("skycorbkg2_tmp", "NAXIS1", yes) | scan (j)
		imexpr ("repl(median(a),b)", "skycorbkg3_tmp",
		"skycorbkg2_tmp", j)
	    }
	    ;

	    # Subtract the pattern bias.
	    # Type 3 also subtracts the background structure from ACE.
	    if (type == 0)
		imcopy (in//sec, im, v-)
	    else if (type == 1 || type == 2)
		imexpr ("a-(abs(b)<c?b:(abs(b)>2*c?0:(b<0?-c:c)))",
		    im, in//sec, "skycorbkg3_tmp", gainnorm)
	    else if (type == 3) {
	        imstat ("skycorsky_tmp", fields="midpt", format-) | scan (x)
		imexpr ("a-(abs(b)<c?b:(abs(b)>2*c?0:(b<0?-c:c)))-(d-e)",
		    im, in//sec, "skycorbkg3_tmp", gainnorm, "skycorsky_tmp", x)
	    }
	    ;

	    # Save resulting background info for final header.
	    mimstat ("skycorsky_tmp"//stat, imask="skycorskyobm_tmp[1]",
		field="mode", format-) | scan (x)
	    printf ("%s%s %g\n", out, sec, x, >> "skycor_mean.tmp")
	    printf ("%g\n", x, >> "skycor_mean2.tmp")
	}
	;

	# Clean up.
	imdelete ("skycor![12bmsRL]*_tmp.*")
    }
    fd = ""

imdel (out)

    # Put the amplifiers back together into a CCD image and delete amp data.
    imcombine ("@skycor_amps.tmp", out, imcmb="", offset="physical", logfile="")
    hedit (out, "NCOMBINE", del+)
    hedit (out, "BPM", bpm, add+)
    print (out, >> "skycor_out.tmp")
    delete ("@skycor_amps.tmp,skycor_amps.tmp")

    # Document.
    printf ("%d %d %d %d %d %d\n",
        xwin, ywin, yshift, ywinobm, splineord, type) | scan (line)
    nhedit (out, "SKYPARS", line, "Sky algorithm parameters", add+)
    print (out//"\n"//sky, >> dataset.fdelete)
    print (sky//"\n"//obm, >> ".skydone_delete.tmp")

}
list = ""

plexit COMPLETED
